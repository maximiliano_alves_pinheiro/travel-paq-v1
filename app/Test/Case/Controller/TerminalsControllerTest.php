<?php
App::uses('TerminalsController', 'Controller');

/**
 * TerminalsController Test Case
 *
 */
class TerminalsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.terminal',
		'app.place',
		'app.airport',
		'app.port',
		'app.cruise',
		'app.cruise_company',
		'app.water_transport',
		'app.cruises_place',
		'app.package',
		'app.operator',
		'app.user',
		'app.category',
		'app.categories_package',
		'app.packages_place'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}

<?php
App::uses('AppModel', 'Model');
/**
 * Place Model
 *
 * @property Airport $Airport
 * @property Port $Port
 * @property Terminal $Terminal
 * @property Cruise $Cruise
 * @property Package $Package
 */
class Pax extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'firstname';
	public $useTable = "paxes";

}

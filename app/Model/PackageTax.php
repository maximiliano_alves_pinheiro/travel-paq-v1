<?php
App::uses('AppModel', 'Model');
/**
 * PackageTax Model
 *
 * @property TaxPackageKind $TaxPackageKind
 */
class PackageTax extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'package_taxs';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'price';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'currency' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'price' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tax_package_kind_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'deleted' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'TaxPackageKind' => array(
			'className' => 'TaxPackageKind',
			'foreignKey' => 'tax_package_kind_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

<div class="operators index">
	<h2><?php echo __('Operators'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('cuit'); ?></th>
			<th><?php echo $this->Paginator->sort('address'); ?></th>
			<th><?php echo $this->Paginator->sort('number_vendors'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($operators as $operator): ?>
	<tr>
		<td><?php echo h($operator['Operator']['id']); ?>&nbsp;</td>
		<td><?php echo h($operator['Operator']['name']); ?>&nbsp;</td>
		<td><?php echo h($operator['Operator']['cuit']); ?>&nbsp;</td>
		<td><?php echo h($operator['Operator']['address']); ?>&nbsp;</td>
		<td><?php echo h($operator['Operator']['number_vendors']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($operator['User']['id'], array('controller' => 'users', 'action' => 'view', $operator['User']['id'])); ?>
		</td>
		<td><?php echo h($operator['Operator']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($operator['Operator']['created']); ?>&nbsp;</td>
		<td><?php echo h($operator['Operator']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $operator['Operator']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $operator['Operator']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $operator['Operator']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $operator['Operator']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Operator'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages'), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package'), array('controller' => 'packages', 'action' => 'add')); ?> </li>
	</ul>
</div>

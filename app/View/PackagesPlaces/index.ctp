<div class="packagesPlaces index">
	<h2><?php echo __('Packages Places'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('number_night'); ?></th>
			<th><?php echo $this->Paginator->sort('place_id'); ?></th>
			<th><?php echo $this->Paginator->sort('package_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($packagesPlaces as $packagesPlace): ?>
	<tr>
		<td><?php echo h($packagesPlace['PackagesPlace']['id']); ?>&nbsp;</td>
		<td><?php echo h($packagesPlace['PackagesPlace']['number_night']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($packagesPlace['Place']['name'], array('controller' => 'places', 'action' => 'view', $packagesPlace['Place']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($packagesPlace['Package']['title'], array('controller' => 'packages', 'action' => 'view', $packagesPlace['Package']['id'])); ?>
		</td>
		<td><?php echo h($packagesPlace['PackagesPlace']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($packagesPlace['PackagesPlace']['created']); ?>&nbsp;</td>
		<td><?php echo h($packagesPlace['PackagesPlace']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $packagesPlace['PackagesPlace']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $packagesPlace['PackagesPlace']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $packagesPlace['PackagesPlace']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $packagesPlace['PackagesPlace']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Packages Place'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages'), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package'), array('controller' => 'packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accommodations Hotels'), array('controller' => 'accommodations_hotels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accommodations Hotel'), array('controller' => 'accommodations_hotels', 'action' => 'add')); ?> </li>
	</ul>
</div>

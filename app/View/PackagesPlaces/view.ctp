<div class="packagesPlaces view">
<h2><?php echo __('Packages Place'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($packagesPlace['PackagesPlace']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number Night'); ?></dt>
		<dd>
			<?php echo h($packagesPlace['PackagesPlace']['number_night']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Place'); ?></dt>
		<dd>
			<?php echo $this->Html->link($packagesPlace['Place']['name'], array('controller' => 'places', 'action' => 'view', $packagesPlace['Place']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Package'); ?></dt>
		<dd>
			<?php echo $this->Html->link($packagesPlace['Package']['title'], array('controller' => 'packages', 'action' => 'view', $packagesPlace['Package']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($packagesPlace['PackagesPlace']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($packagesPlace['PackagesPlace']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($packagesPlace['PackagesPlace']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Packages Place'), array('action' => 'edit', $packagesPlace['PackagesPlace']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Packages Place'), array('action' => 'delete', $packagesPlace['PackagesPlace']['id']), array(), __('Are you sure you want to delete # %s?', $packagesPlace['PackagesPlace']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages Places'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Packages Place'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages'), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package'), array('controller' => 'packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accommodations Hotels'), array('controller' => 'accommodations_hotels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accommodations Hotel'), array('controller' => 'accommodations_hotels', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Accommodations Hotels'); ?></h3>
	<?php if (!empty($packagesPlace['AccommodationsHotel'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Accommodation Id'); ?></th>
		<th><?php echo __('Hotel Id'); ?></th>
		<th><?php echo __('Packages Place Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($packagesPlace['AccommodationsHotel'] as $accommodationsHotel): ?>
		<tr>
			<td><?php echo $accommodationsHotel['id']; ?></td>
			<td><?php echo $accommodationsHotel['accommodation_id']; ?></td>
			<td><?php echo $accommodationsHotel['hotel_id']; ?></td>
			<td><?php echo $accommodationsHotel['packages_place_id']; ?></td>
			<td><?php echo $accommodationsHotel['deleted']; ?></td>
			<td><?php echo $accommodationsHotel['created']; ?></td>
			<td><?php echo $accommodationsHotel['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'accommodations_hotels', 'action' => 'view', $accommodationsHotel['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'accommodations_hotels', 'action' => 'edit', $accommodationsHotel['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'accommodations_hotels', 'action' => 'delete', $accommodationsHotel['id']), array(), __('Are you sure you want to delete # %s?', $accommodationsHotel['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Accommodations Hotel'), array('controller' => 'accommodations_hotels', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

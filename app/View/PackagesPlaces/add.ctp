<div class="packagesPlaces form">
<?php echo $this->Form->create('PackagesPlace'); ?>
	<fieldset>
		<legend><?php echo __('Add Packages Place'); ?></legend>
	<?php
		echo $this->Form->input('number_night');
		echo $this->Form->input('place_id');
		echo $this->Form->input('package_id');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Packages Places'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages'), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package'), array('controller' => 'packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accommodations Hotels'), array('controller' => 'accommodations_hotels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accommodations Hotel'), array('controller' => 'accommodations_hotels', 'action' => 'add')); ?> </li>
	</ul>
</div>

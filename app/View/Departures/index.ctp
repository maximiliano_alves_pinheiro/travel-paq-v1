<div class="departures index">
	<h2><?php echo __('Departures'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($departures as $departure): ?>
	<tr>
		<td><?php echo h($departure['Departure']['id']); ?>&nbsp;</td>
		<td><?php echo h($departure['Departure']['date']); ?>&nbsp;</td>
		<td><?php echo h($departure['Departure']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($departure['Departure']['created']); ?>&nbsp;</td>
		<td><?php echo h($departure['Departure']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $departure['Departure']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $departure['Departure']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $departure['Departure']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $departure['Departure']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Departure'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Air Transports'), array('controller' => 'air_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Air Transport'), array('controller' => 'air_transports', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ground Transports'), array('controller' => 'ground_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ground Transport'), array('controller' => 'ground_transports', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Water Transports'), array('controller' => 'water_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Water Transport'), array('controller' => 'water_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="departures view">
<h2><?php echo __('Departure'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($departure['Departure']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($departure['Departure']['date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($departure['Departure']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($departure['Departure']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($departure['Departure']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Departure'), array('action' => 'edit', $departure['Departure']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Departure'), array('action' => 'delete', $departure['Departure']['id']), array(), __('Are you sure you want to delete # %s?', $departure['Departure']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Departures'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Departure'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Air Transports'), array('controller' => 'air_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Air Transport'), array('controller' => 'air_transports', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ground Transports'), array('controller' => 'ground_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ground Transport'), array('controller' => 'ground_transports', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Water Transports'), array('controller' => 'water_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Water Transport'), array('controller' => 'water_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Air Transports'); ?></h3>
	<?php if (!empty($departure['AirTransport'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Flight Number'); ?></th>
		<th><?php echo __('Order Number'); ?></th>
		<th><?php echo __('Departure Time'); ?></th>
		<th><?php echo __('Arrival Time'); ?></th>
		<th><?php echo __('Airline Id'); ?></th>
		<th><?php echo __('Departure Airport Id'); ?></th>
		<th><?php echo __('Arrival Airport Id'); ?></th>
		<th><?php echo __('Departure Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($departure['AirTransport'] as $airTransport): ?>
		<tr>
			<td><?php echo $airTransport['id']; ?></td>
			<td><?php echo $airTransport['flight_number']; ?></td>
			<td><?php echo $airTransport['order_number']; ?></td>
			<td><?php echo $airTransport['departure_time']; ?></td>
			<td><?php echo $airTransport['arrival_time']; ?></td>
			<td><?php echo $airTransport['airline_id']; ?></td>
			<td><?php echo $airTransport['departure_airport_id']; ?></td>
			<td><?php echo $airTransport['arrival_airport_id']; ?></td>
			<td><?php echo $airTransport['departure_id']; ?></td>
			<td><?php echo $airTransport['deleted']; ?></td>
			<td><?php echo $airTransport['created']; ?></td>
			<td><?php echo $airTransport['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'air_transports', 'action' => 'view', $airTransport['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'air_transports', 'action' => 'edit', $airTransport['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'air_transports', 'action' => 'delete', $airTransport['id']), array(), __('Are you sure you want to delete # %s?', $airTransport['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Air Transport'), array('controller' => 'air_transports', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Ground Transports'); ?></h3>
	<?php if (!empty($departure['GroundTransport'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Travel Number'); ?></th>
		<th><?php echo __('Arrival Time'); ?></th>
		<th><?php echo __('Departure Time'); ?></th>
		<th><?php echo __('Departure Terminal Id'); ?></th>
		<th><?php echo __('Arrival Terminal Id'); ?></th>
		<th><?php echo __('Bus Company Id'); ?></th>
		<th><?php echo __('Departure Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($departure['GroundTransport'] as $groundTransport): ?>
		<tr>
			<td><?php echo $groundTransport['id']; ?></td>
			<td><?php echo $groundTransport['travel_number']; ?></td>
			<td><?php echo $groundTransport['arrival_time']; ?></td>
			<td><?php echo $groundTransport['departure_time']; ?></td>
			<td><?php echo $groundTransport['departure_terminal_id']; ?></td>
			<td><?php echo $groundTransport['arrival_terminal_id']; ?></td>
			<td><?php echo $groundTransport['bus_company_id']; ?></td>
			<td><?php echo $groundTransport['departure_id']; ?></td>
			<td><?php echo $groundTransport['deleted']; ?></td>
			<td><?php echo $groundTransport['created']; ?></td>
			<td><?php echo $groundTransport['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ground_transports', 'action' => 'view', $groundTransport['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ground_transports', 'action' => 'edit', $groundTransport['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ground_transports', 'action' => 'delete', $groundTransport['id']), array(), __('Are you sure you want to delete # %s?', $groundTransport['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ground Transport'), array('controller' => 'ground_transports', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Water Transports'); ?></h3>
	<?php if (!empty($departure['WaterTransport'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Travel Number'); ?></th>
		<th><?php echo __('Cruise Company Id'); ?></th>
		<th><?php echo __('Departure Port Id'); ?></th>
		<th><?php echo __('Arrival Port Id'); ?></th>
		<th><?php echo __('Departure Time'); ?></th>
		<th><?php echo __('Arrival Time'); ?></th>
		<th><?php echo __('Departure Id'); ?></th>
		<th><?php echo __('Cruises Name'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($departure['WaterTransport'] as $waterTransport): ?>
		<tr>
			<td><?php echo $waterTransport['id']; ?></td>
			<td><?php echo $waterTransport['travel_number']; ?></td>
			<td><?php echo $waterTransport['cruise_company_id']; ?></td>
			<td><?php echo $waterTransport['departure_port_id']; ?></td>
			<td><?php echo $waterTransport['arrival_port_id']; ?></td>
			<td><?php echo $waterTransport['departure_time']; ?></td>
			<td><?php echo $waterTransport['arrival_time']; ?></td>
			<td><?php echo $waterTransport['departure_id']; ?></td>
			<td><?php echo $waterTransport['cruises_name']; ?></td>
			<td><?php echo $waterTransport['deleted']; ?></td>
			<td><?php echo $waterTransport['created']; ?></td>
			<td><?php echo $waterTransport['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'water_transports', 'action' => 'view', $waterTransport['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'water_transports', 'action' => 'edit', $waterTransport['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'water_transports', 'action' => 'delete', $waterTransport['id']), array(), __('Are you sure you want to delete # %s?', $waterTransport['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Water Transport'), array('controller' => 'water_transports', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

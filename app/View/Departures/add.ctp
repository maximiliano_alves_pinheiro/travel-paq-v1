<div class="departures form">
<?php echo $this->Form->create('Departure'); ?>
	<fieldset>
		<legend><?php echo __('Add Departure'); ?></legend>
	<?php
		echo $this->Form->input('date');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Departures'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Air Transports'), array('controller' => 'air_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Air Transport'), array('controller' => 'air_transports', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ground Transports'), array('controller' => 'ground_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ground Transport'), array('controller' => 'ground_transports', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Water Transports'), array('controller' => 'water_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Water Transport'), array('controller' => 'water_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>

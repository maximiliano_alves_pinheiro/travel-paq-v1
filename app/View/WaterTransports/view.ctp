<div class="waterTransports view">
<h2><?php echo __('Water Transport'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($waterTransport['WaterTransport']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Travel Number'); ?></dt>
		<dd>
			<?php echo h($waterTransport['WaterTransport']['travel_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cruise Company'); ?></dt>
		<dd>
			<?php echo $this->Html->link($waterTransport['CruiseCompany']['name'], array('controller' => 'cruise_companies', 'action' => 'view', $waterTransport['CruiseCompany']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure Port Id'); ?></dt>
		<dd>
			<?php echo h($waterTransport['WaterTransport']['departure_port_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Arrival Port Id'); ?></dt>
		<dd>
			<?php echo h($waterTransport['WaterTransport']['arrival_port_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure Time'); ?></dt>
		<dd>
			<?php echo h($waterTransport['WaterTransport']['departure_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Arrival Time'); ?></dt>
		<dd>
			<?php echo h($waterTransport['WaterTransport']['arrival_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure'); ?></dt>
		<dd>
			<?php echo $this->Html->link($waterTransport['Departure']['date'], array('controller' => 'departures', 'action' => 'view', $waterTransport['Departure']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cruises Name'); ?></dt>
		<dd>
			<?php echo h($waterTransport['WaterTransport']['cruises_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($waterTransport['WaterTransport']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($waterTransport['WaterTransport']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($waterTransport['WaterTransport']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Water Transport'), array('action' => 'edit', $waterTransport['WaterTransport']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Water Transport'), array('action' => 'delete', $waterTransport['WaterTransport']['id']), array(), __('Are you sure you want to delete # %s?', $waterTransport['WaterTransport']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Water Transports'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Water Transport'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cruise Companies'), array('controller' => 'cruise_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise Company'), array('controller' => 'cruise_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departures'), array('controller' => 'departures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Departure'), array('controller' => 'departures', 'action' => 'add')); ?> </li>
	</ul>
</div>

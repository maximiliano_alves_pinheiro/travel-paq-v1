<div class="waterTransports form">
<?php echo $this->Form->create('WaterTransport'); ?>
	<fieldset>
		<legend><?php echo __('Edit Water Transport'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('travel_number');
		echo $this->Form->input('cruise_company_id');
		echo $this->Form->input('departure_port_id');
		echo $this->Form->input('arrival_port_id');
		echo $this->Form->input('departure_time');
		echo $this->Form->input('arrival_time');
		echo $this->Form->input('departure_id');
		echo $this->Form->input('cruises_name');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('WaterTransport.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('WaterTransport.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Water Transports'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Cruise Companies'), array('controller' => 'cruise_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise Company'), array('controller' => 'cruise_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departures'), array('controller' => 'departures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Departure'), array('controller' => 'departures', 'action' => 'add')); ?> </li>
	</ul>
</div>

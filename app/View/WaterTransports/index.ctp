<div class="waterTransports index">
	<h2><?php echo __('Water Transports'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('travel_number'); ?></th>
			<th><?php echo $this->Paginator->sort('cruise_company_id'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_port_id'); ?></th>
			<th><?php echo $this->Paginator->sort('arrival_port_id'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_time'); ?></th>
			<th><?php echo $this->Paginator->sort('arrival_time'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_id'); ?></th>
			<th><?php echo $this->Paginator->sort('cruises_name'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($waterTransports as $waterTransport): ?>
	<tr>
		<td><?php echo h($waterTransport['WaterTransport']['id']); ?>&nbsp;</td>
		<td><?php echo h($waterTransport['WaterTransport']['travel_number']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($waterTransport['CruiseCompany']['name'], array('controller' => 'cruise_companies', 'action' => 'view', $waterTransport['CruiseCompany']['id'])); ?>
		</td>
		<td><?php echo h($waterTransport['WaterTransport']['departure_port_id']); ?>&nbsp;</td>
		<td><?php echo h($waterTransport['WaterTransport']['arrival_port_id']); ?>&nbsp;</td>
		<td><?php echo h($waterTransport['WaterTransport']['departure_time']); ?>&nbsp;</td>
		<td><?php echo h($waterTransport['WaterTransport']['arrival_time']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($waterTransport['Departure']['date'], array('controller' => 'departures', 'action' => 'view', $waterTransport['Departure']['id'])); ?>
		</td>
		<td><?php echo h($waterTransport['WaterTransport']['cruises_name']); ?>&nbsp;</td>
		<td><?php echo h($waterTransport['WaterTransport']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($waterTransport['WaterTransport']['created']); ?>&nbsp;</td>
		<td><?php echo h($waterTransport['WaterTransport']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $waterTransport['WaterTransport']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $waterTransport['WaterTransport']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $waterTransport['WaterTransport']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $waterTransport['WaterTransport']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Water Transport'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Cruise Companies'), array('controller' => 'cruise_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise Company'), array('controller' => 'cruise_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departures'), array('controller' => 'departures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Departure'), array('controller' => 'departures', 'action' => 'add')); ?> </li>
	</ul>
</div>

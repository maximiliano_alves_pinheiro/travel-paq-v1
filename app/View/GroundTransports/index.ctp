<div class="groundTransports index">
	<h2><?php echo __('Ground Transports'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('travel_number'); ?></th>
			<th><?php echo $this->Paginator->sort('arrival_time'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_time'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_terminal_id'); ?></th>
			<th><?php echo $this->Paginator->sort('arrival_terminal_id'); ?></th>
			<th><?php echo $this->Paginator->sort('bus_company_id'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($groundTransports as $groundTransport): ?>
	<tr>
		<td><?php echo h($groundTransport['GroundTransport']['id']); ?>&nbsp;</td>
		<td><?php echo h($groundTransport['GroundTransport']['travel_number']); ?>&nbsp;</td>
		<td><?php echo h($groundTransport['GroundTransport']['arrival_time']); ?>&nbsp;</td>
		<td><?php echo h($groundTransport['GroundTransport']['departure_time']); ?>&nbsp;</td>
		<td><?php echo h($groundTransport['GroundTransport']['departure_terminal_id']); ?>&nbsp;</td>
		<td><?php echo h($groundTransport['GroundTransport']['arrival_terminal_id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($groundTransport['BusCompany']['name'], array('controller' => 'bus_companies', 'action' => 'view', $groundTransport['BusCompany']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($groundTransport['Departure']['date'], array('controller' => 'departures', 'action' => 'view', $groundTransport['Departure']['id'])); ?>
		</td>
		<td><?php echo h($groundTransport['GroundTransport']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($groundTransport['GroundTransport']['created']); ?>&nbsp;</td>
		<td><?php echo h($groundTransport['GroundTransport']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $groundTransport['GroundTransport']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $groundTransport['GroundTransport']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $groundTransport['GroundTransport']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $groundTransport['GroundTransport']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Ground Transport'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Bus Companies'), array('controller' => 'bus_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bus Company'), array('controller' => 'bus_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departures'), array('controller' => 'departures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Departure'), array('controller' => 'departures', 'action' => 'add')); ?> </li>
	</ul>
</div>

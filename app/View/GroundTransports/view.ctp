<div class="groundTransports view">
<h2><?php echo __('Ground Transport'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($groundTransport['GroundTransport']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Travel Number'); ?></dt>
		<dd>
			<?php echo h($groundTransport['GroundTransport']['travel_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Arrival Time'); ?></dt>
		<dd>
			<?php echo h($groundTransport['GroundTransport']['arrival_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure Time'); ?></dt>
		<dd>
			<?php echo h($groundTransport['GroundTransport']['departure_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure Terminal Id'); ?></dt>
		<dd>
			<?php echo h($groundTransport['GroundTransport']['departure_terminal_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Arrival Terminal Id'); ?></dt>
		<dd>
			<?php echo h($groundTransport['GroundTransport']['arrival_terminal_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bus Company'); ?></dt>
		<dd>
			<?php echo $this->Html->link($groundTransport['BusCompany']['name'], array('controller' => 'bus_companies', 'action' => 'view', $groundTransport['BusCompany']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure'); ?></dt>
		<dd>
			<?php echo $this->Html->link($groundTransport['Departure']['date'], array('controller' => 'departures', 'action' => 'view', $groundTransport['Departure']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($groundTransport['GroundTransport']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($groundTransport['GroundTransport']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($groundTransport['GroundTransport']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ground Transport'), array('action' => 'edit', $groundTransport['GroundTransport']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ground Transport'), array('action' => 'delete', $groundTransport['GroundTransport']['id']), array(), __('Are you sure you want to delete # %s?', $groundTransport['GroundTransport']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ground Transports'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ground Transport'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bus Companies'), array('controller' => 'bus_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bus Company'), array('controller' => 'bus_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departures'), array('controller' => 'departures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Departure'), array('controller' => 'departures', 'action' => 'add')); ?> </li>
	</ul>
</div>

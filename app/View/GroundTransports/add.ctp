<div class="groundTransports form">
<?php echo $this->Form->create('GroundTransport'); ?>
	<fieldset>
		<legend><?php echo __('Add Ground Transport'); ?></legend>
	<?php
		echo $this->Form->input('travel_number');
		echo $this->Form->input('arrival_time');
		echo $this->Form->input('departure_time');
		echo $this->Form->input('departure_terminal_id');
		echo $this->Form->input('arrival_terminal_id');
		echo $this->Form->input('bus_company_id');
		echo $this->Form->input('departure_id');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Ground Transports'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Bus Companies'), array('controller' => 'bus_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bus Company'), array('controller' => 'bus_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departures'), array('controller' => 'departures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Departure'), array('controller' => 'departures', 'action' => 'add')); ?> </li>
	</ul>
</div>

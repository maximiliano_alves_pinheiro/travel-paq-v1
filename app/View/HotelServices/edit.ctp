<div class="hotelServices form">
<?php echo $this->Form->create('HotelService'); ?>
	<fieldset>
		<legend><?php echo __('Edit Hotel Service'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('HotelService.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('HotelService.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Hotel Services'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Accommodations'), array('controller' => 'accommodations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accommodation'), array('controller' => 'accommodations', 'action' => 'add')); ?> </li>
	</ul>
</div>

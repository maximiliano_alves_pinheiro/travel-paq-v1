<div class="packageTaxes form">
<?php echo $this->Form->create('PackageTax'); ?>
	<fieldset>
		<legend><?php echo __('Add Package Tax'); ?></legend>
	<?php
		echo $this->Form->input('currency');
		echo $this->Form->input('price');
		echo $this->Form->input('tax_package_kind_id');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Package Taxes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Tax Package Kinds'), array('controller' => 'tax_package_kinds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tax Package Kind'), array('controller' => 'tax_package_kinds', 'action' => 'add')); ?> </li>
	</ul>
</div>

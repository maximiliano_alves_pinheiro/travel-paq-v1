<div class="packageTaxes view">
<h2><?php echo __('Package Tax'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($packageTax['PackageTax']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Currency'); ?></dt>
		<dd>
			<?php echo h($packageTax['PackageTax']['currency']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($packageTax['PackageTax']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tax Package Kind'); ?></dt>
		<dd>
			<?php echo $this->Html->link($packageTax['TaxPackageKind']['name'], array('controller' => 'tax_package_kinds', 'action' => 'view', $packageTax['TaxPackageKind']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($packageTax['PackageTax']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($packageTax['PackageTax']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($packageTax['PackageTax']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Package Tax'), array('action' => 'edit', $packageTax['PackageTax']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Package Tax'), array('action' => 'delete', $packageTax['PackageTax']['id']), array(), __('Are you sure you want to delete # %s?', $packageTax['PackageTax']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Package Taxes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package Tax'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tax Package Kinds'), array('controller' => 'tax_package_kinds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tax Package Kind'), array('controller' => 'tax_package_kinds', 'action' => 'add')); ?> </li>
	</ul>
</div>

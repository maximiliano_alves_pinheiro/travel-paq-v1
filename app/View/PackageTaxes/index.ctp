<div class="packageTaxes index">
	<h2><?php echo __('Package Taxes'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('currency'); ?></th>
			<th><?php echo $this->Paginator->sort('price'); ?></th>
			<th><?php echo $this->Paginator->sort('tax_package_kind_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($packageTaxes as $packageTax): ?>
	<tr>
		<td><?php echo h($packageTax['PackageTax']['id']); ?>&nbsp;</td>
		<td><?php echo h($packageTax['PackageTax']['currency']); ?>&nbsp;</td>
		<td><?php echo h($packageTax['PackageTax']['price']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($packageTax['TaxPackageKind']['name'], array('controller' => 'tax_package_kinds', 'action' => 'view', $packageTax['TaxPackageKind']['id'])); ?>
		</td>
		<td><?php echo h($packageTax['PackageTax']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($packageTax['PackageTax']['created']); ?>&nbsp;</td>
		<td><?php echo h($packageTax['PackageTax']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $packageTax['PackageTax']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $packageTax['PackageTax']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $packageTax['PackageTax']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $packageTax['PackageTax']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Package Tax'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Tax Package Kinds'), array('controller' => 'tax_package_kinds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tax Package Kind'), array('controller' => 'tax_package_kinds', 'action' => 'add')); ?> </li>
	</ul>
</div>

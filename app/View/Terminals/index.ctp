<div class="terminals index">
	<h2><?php echo __('Terminals'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('place_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($terminals as $terminal): ?>
	<tr>
		<td><?php echo h($terminal['Terminal']['id']); ?>&nbsp;</td>
		<td><?php echo h($terminal['Terminal']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($terminal['Place']['name'], array('controller' => 'places', 'action' => 'view', $terminal['Place']['id'])); ?>
		</td>
		<td><?php echo h($terminal['Terminal']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($terminal['Terminal']['created']); ?>&nbsp;</td>
		<td><?php echo h($terminal['Terminal']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $terminal['Terminal']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $terminal['Terminal']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $terminal['Terminal']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $terminal['Terminal']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Terminal'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
	</ul>
</div>

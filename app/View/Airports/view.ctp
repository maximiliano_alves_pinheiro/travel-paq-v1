<div class="airports view">
<h2><?php echo __('Airport'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($airport['Airport']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($airport['Airport']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($airport['Airport']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Place'); ?></dt>
		<dd>
			<?php echo $this->Html->link($airport['Place']['name'], array('controller' => 'places', 'action' => 'view', $airport['Place']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($airport['Airport']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($airport['Airport']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($airport['Airport']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Airport'), array('action' => 'edit', $airport['Airport']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Airport'), array('action' => 'delete', $airport['Airport']['id']), array(), __('Are you sure you want to delete # %s?', $airport['Airport']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Airports'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Airport'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="ports view">
<h2><?php echo __('Port'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($port['Port']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($port['Port']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Place'); ?></dt>
		<dd>
			<?php echo $this->Html->link($port['Place']['name'], array('controller' => 'places', 'action' => 'view', $port['Place']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($port['Port']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($port['Port']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($port['Port']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Port'), array('action' => 'edit', $port['Port']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Port'), array('action' => 'delete', $port['Port']['id']), array(), __('Are you sure you want to delete # %s?', $port['Port']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ports'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Port'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
	</ul>
</div>

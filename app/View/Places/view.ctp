<div class="places view">
<h2><?php echo __('Place'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($place['Place']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($place['Place']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo h($place['Place']['country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Region'); ?></dt>
		<dd>
			<?php echo h($place['Place']['region']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($place['Place']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($place['Place']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($place['Place']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Place'), array('action' => 'edit', $place['Place']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Place'), array('action' => 'delete', $place['Place']['id']), array(), __('Are you sure you want to delete # %s?', $place['Place']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Airports'), array('controller' => 'airports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Airport'), array('controller' => 'airports', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ports'), array('controller' => 'ports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Port'), array('controller' => 'ports', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Terminals'), array('controller' => 'terminals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Terminal'), array('controller' => 'terminals', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cruises'), array('controller' => 'cruises', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise'), array('controller' => 'cruises', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages'), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package'), array('controller' => 'packages', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Airports'); ?></h3>
	<?php if (!empty($place['Airport'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Place Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($place['Airport'] as $airport): ?>
		<tr>
			<td><?php echo $airport['id']; ?></td>
			<td><?php echo $airport['code']; ?></td>
			<td><?php echo $airport['name']; ?></td>
			<td><?php echo $airport['place_id']; ?></td>
			<td><?php echo $airport['deleted']; ?></td>
			<td><?php echo $airport['created']; ?></td>
			<td><?php echo $airport['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'airports', 'action' => 'view', $airport['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'airports', 'action' => 'edit', $airport['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'airports', 'action' => 'delete', $airport['id']), array(), __('Are you sure you want to delete # %s?', $airport['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Airport'), array('controller' => 'airports', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Ports'); ?></h3>
	<?php if (!empty($place['Port'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Place Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($place['Port'] as $port): ?>
		<tr>
			<td><?php echo $port['id']; ?></td>
			<td><?php echo $port['name']; ?></td>
			<td><?php echo $port['place_id']; ?></td>
			<td><?php echo $port['deleted']; ?></td>
			<td><?php echo $port['created']; ?></td>
			<td><?php echo $port['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ports', 'action' => 'view', $port['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ports', 'action' => 'edit', $port['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ports', 'action' => 'delete', $port['id']), array(), __('Are you sure you want to delete # %s?', $port['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Port'), array('controller' => 'ports', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Terminals'); ?></h3>
	<?php if (!empty($place['Terminal'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Place Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($place['Terminal'] as $terminal): ?>
		<tr>
			<td><?php echo $terminal['id']; ?></td>
			<td><?php echo $terminal['name']; ?></td>
			<td><?php echo $terminal['place_id']; ?></td>
			<td><?php echo $terminal['deleted']; ?></td>
			<td><?php echo $terminal['created']; ?></td>
			<td><?php echo $terminal['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'terminals', 'action' => 'view', $terminal['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'terminals', 'action' => 'edit', $terminal['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'terminals', 'action' => 'delete', $terminal['id']), array(), __('Are you sure you want to delete # %s?', $terminal['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Terminal'), array('controller' => 'terminals', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Cruises'); ?></h3>
	<?php if (!empty($place['Cruise'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Cruises Name'); ?></th>
		<th><?php echo __('Number Nights'); ?></th>
		<th><?php echo __('Camarote'); ?></th>
		<th><?php echo __('Service On Board'); ?></th>
		<th><?php echo __('Arrival Time'); ?></th>
		<th><?php echo __('Departure Time'); ?></th>
		<th><?php echo __('Departure Port Id'); ?></th>
		<th><?php echo __('Arrivial Port Id'); ?></th>
		<th><?php echo __('Cruise Company Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($place['Cruise'] as $cruise): ?>
		<tr>
			<td><?php echo $cruise['id']; ?></td>
			<td><?php echo $cruise['cruises_name']; ?></td>
			<td><?php echo $cruise['number_nights']; ?></td>
			<td><?php echo $cruise['camarote']; ?></td>
			<td><?php echo $cruise['service_on_board']; ?></td>
			<td><?php echo $cruise['arrival_time']; ?></td>
			<td><?php echo $cruise['departure_time']; ?></td>
			<td><?php echo $cruise['departure_port_id']; ?></td>
			<td><?php echo $cruise['arrivial_port_id']; ?></td>
			<td><?php echo $cruise['cruise_company_id']; ?></td>
			<td><?php echo $cruise['created']; ?></td>
			<td><?php echo $cruise['modified']; ?></td>
			<td><?php echo $cruise['deleted']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'cruises', 'action' => 'view', $cruise['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'cruises', 'action' => 'edit', $cruise['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'cruises', 'action' => 'delete', $cruise['id']), array(), __('Are you sure you want to delete # %s?', $cruise['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Cruise'), array('controller' => 'cruises', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Packages'); ?></h3>
	<?php if (!empty($place['Package'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Start Date'); ?></th>
		<th><?php echo __('Expired Date'); ?></th>
		<th><?php echo __('Observations'); ?></th>
		<th><?php echo __('Operator Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($place['Package'] as $package): ?>
		<tr>
			<td><?php echo $package['id']; ?></td>
			<td><?php echo $package['title']; ?></td>
			<td><?php echo $package['start_date']; ?></td>
			<td><?php echo $package['expired_date']; ?></td>
			<td><?php echo $package['observations']; ?></td>
			<td><?php echo $package['operator_id']; ?></td>
			<td><?php echo $package['deleted']; ?></td>
			<td><?php echo $package['created']; ?></td>
			<td><?php echo $package['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'packages', 'action' => 'view', $package['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'packages', 'action' => 'edit', $package['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'packages', 'action' => 'delete', $package['id']), array(), __('Are you sure you want to delete # %s?', $package['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Package'), array('controller' => 'packages', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

<div class="hotels form">
<?php echo $this->Form->create('Hotel'); ?>
	<fieldset>
		<legend><?php echo __('Add Hotel'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('maxi_children_age');
		echo $this->Form->input('deleted');
		echo $this->Form->input('Accommodation');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Hotels'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Accommodations'), array('controller' => 'accommodations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accommodation'), array('controller' => 'accommodations', 'action' => 'add')); ?> </li>
	</ul>
</div>

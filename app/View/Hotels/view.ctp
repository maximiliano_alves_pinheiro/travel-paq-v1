<div class="hotels view">
<h2><?php echo __('Hotel'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($hotel['Hotel']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($hotel['Hotel']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Maxi Children Age'); ?></dt>
		<dd>
			<?php echo h($hotel['Hotel']['maxi_children_age']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($hotel['Hotel']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($hotel['Hotel']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($hotel['Hotel']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Hotel'), array('action' => 'edit', $hotel['Hotel']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Hotel'), array('action' => 'delete', $hotel['Hotel']['id']), array(), __('Are you sure you want to delete # %s?', $hotel['Hotel']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Hotels'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Hotel'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accommodations'), array('controller' => 'accommodations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accommodation'), array('controller' => 'accommodations', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Accommodations'); ?></h3>
	<?php if (!empty($hotel['Accommodation'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Hotel Service Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($hotel['Accommodation'] as $accommodation): ?>
		<tr>
			<td><?php echo $accommodation['id']; ?></td>
			<td><?php echo $accommodation['hotel_service_id']; ?></td>
			<td><?php echo $accommodation['deleted']; ?></td>
			<td><?php echo $accommodation['created']; ?></td>
			<td><?php echo $accommodation['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'accommodations', 'action' => 'view', $accommodation['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'accommodations', 'action' => 'edit', $accommodation['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'accommodations', 'action' => 'delete', $accommodation['id']), array(), __('Are you sure you want to delete # %s?', $accommodation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Accommodation'), array('controller' => 'accommodations', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

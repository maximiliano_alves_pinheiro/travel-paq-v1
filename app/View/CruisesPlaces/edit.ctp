<div class="cruisesPlaces form">
<?php echo $this->Form->create('CruisesPlace'); ?>
	<fieldset>
		<legend><?php echo __('Edit Cruises Place'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('order_number');
		echo $this->Form->input('cruise_id');
		echo $this->Form->input('place_id');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CruisesPlace.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('CruisesPlace.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Cruises Places'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Cruises'), array('controller' => 'cruises', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise'), array('controller' => 'cruises', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
	</ul>
</div>

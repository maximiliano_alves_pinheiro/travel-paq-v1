<div class="cruisesPlaces view">
<h2><?php echo __('Cruises Place'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($cruisesPlace['CruisesPlace']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order Number'); ?></dt>
		<dd>
			<?php echo h($cruisesPlace['CruisesPlace']['order_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cruise'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cruisesPlace['Cruise']['cruises_name'], array('controller' => 'cruises', 'action' => 'view', $cruisesPlace['Cruise']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Place'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cruisesPlace['Place']['name'], array('controller' => 'places', 'action' => 'view', $cruisesPlace['Place']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($cruisesPlace['CruisesPlace']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($cruisesPlace['CruisesPlace']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($cruisesPlace['CruisesPlace']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cruises Place'), array('action' => 'edit', $cruisesPlace['CruisesPlace']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cruises Place'), array('action' => 'delete', $cruisesPlace['CruisesPlace']['id']), array(), __('Are you sure you want to delete # %s?', $cruisesPlace['CruisesPlace']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cruises Places'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruises Place'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cruises'), array('controller' => 'cruises', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise'), array('controller' => 'cruises', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="airlines view">
<h2><?php echo __('Airline'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($airline['Airline']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($airline['Airline']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($airline['Airline']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($airline['Airline']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($airline['Airline']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Airline'), array('action' => 'edit', $airline['Airline']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Airline'), array('action' => 'delete', $airline['Airline']['id']), array(), __('Are you sure you want to delete # %s?', $airline['Airline']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Airlines'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Airline'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Air Transports'), array('controller' => 'air_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Air Transport'), array('controller' => 'air_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Air Transports'); ?></h3>
	<?php if (!empty($airline['AirTransport'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Flight Number'); ?></th>
		<th><?php echo __('Order Number'); ?></th>
		<th><?php echo __('Departure Time'); ?></th>
		<th><?php echo __('Arrival Time'); ?></th>
		<th><?php echo __('Airline Id'); ?></th>
		<th><?php echo __('Departure Airport Id'); ?></th>
		<th><?php echo __('Arrival Airport Id'); ?></th>
		<th><?php echo __('Departure Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($airline['AirTransport'] as $airTransport): ?>
		<tr>
			<td><?php echo $airTransport['id']; ?></td>
			<td><?php echo $airTransport['flight_number']; ?></td>
			<td><?php echo $airTransport['order_number']; ?></td>
			<td><?php echo $airTransport['departure_time']; ?></td>
			<td><?php echo $airTransport['arrival_time']; ?></td>
			<td><?php echo $airTransport['airline_id']; ?></td>
			<td><?php echo $airTransport['departure_airport_id']; ?></td>
			<td><?php echo $airTransport['arrival_airport_id']; ?></td>
			<td><?php echo $airTransport['departure_id']; ?></td>
			<td><?php echo $airTransport['deleted']; ?></td>
			<td><?php echo $airTransport['created']; ?></td>
			<td><?php echo $airTransport['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'air_transports', 'action' => 'view', $airTransport['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'air_transports', 'action' => 'edit', $airTransport['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'air_transports', 'action' => 'delete', $airTransport['id']), array(), __('Are you sure you want to delete # %s?', $airTransport['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Air Transport'), array('controller' => 'air_transports', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

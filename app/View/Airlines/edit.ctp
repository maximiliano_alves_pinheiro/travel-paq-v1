<div class="airlines form">
<?php echo $this->Form->create('Airline'); ?>
	<fieldset>
		<legend><?php echo __('Edit Airline'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Airline.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Airline.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Airlines'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Air Transports'), array('controller' => 'air_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Air Transport'), array('controller' => 'air_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>

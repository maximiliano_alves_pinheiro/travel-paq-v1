<div class="services form">
<?php echo $this->Form->create('Service'); ?>
	<fieldset>
		<legend><?php echo __('Add Service'); ?></legend>
	<?php
		echo $this->Form->input('detail');
		echo $this->Form->input('service_kind_id');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Services'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Service Kinds'), array('controller' => 'service_kinds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service Kind'), array('controller' => 'service_kinds', 'action' => 'add')); ?> </li>
	</ul>
</div>

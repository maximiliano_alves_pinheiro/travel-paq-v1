<div class="cruiseCompanies index">
	<h2><?php echo __('Cruise Companies'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($cruiseCompanies as $cruiseCompany): ?>
	<tr>
		<td><?php echo h($cruiseCompany['CruiseCompany']['id']); ?>&nbsp;</td>
		<td><?php echo h($cruiseCompany['CruiseCompany']['name']); ?>&nbsp;</td>
		<td><?php echo h($cruiseCompany['CruiseCompany']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($cruiseCompany['CruiseCompany']['created']); ?>&nbsp;</td>
		<td><?php echo h($cruiseCompany['CruiseCompany']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $cruiseCompany['CruiseCompany']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $cruiseCompany['CruiseCompany']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $cruiseCompany['CruiseCompany']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $cruiseCompany['CruiseCompany']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Cruise Company'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Cruises'), array('controller' => 'cruises', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise'), array('controller' => 'cruises', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Water Transports'), array('controller' => 'water_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Water Transport'), array('controller' => 'water_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>

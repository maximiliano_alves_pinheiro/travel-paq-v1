<div class="cruiseCompanies form">
<?php echo $this->Form->create('CruiseCompany'); ?>
	<fieldset>
		<legend><?php echo __('Edit Cruise Company'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CruiseCompany.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('CruiseCompany.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Cruise Companies'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Cruises'), array('controller' => 'cruises', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise'), array('controller' => 'cruises', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Water Transports'), array('controller' => 'water_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Water Transport'), array('controller' => 'water_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>

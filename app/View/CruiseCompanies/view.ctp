<div class="cruiseCompanies view">
<h2><?php echo __('Cruise Company'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($cruiseCompany['CruiseCompany']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($cruiseCompany['CruiseCompany']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($cruiseCompany['CruiseCompany']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($cruiseCompany['CruiseCompany']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($cruiseCompany['CruiseCompany']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cruise Company'), array('action' => 'edit', $cruiseCompany['CruiseCompany']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cruise Company'), array('action' => 'delete', $cruiseCompany['CruiseCompany']['id']), array(), __('Are you sure you want to delete # %s?', $cruiseCompany['CruiseCompany']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cruise Companies'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise Company'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cruises'), array('controller' => 'cruises', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise'), array('controller' => 'cruises', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Water Transports'), array('controller' => 'water_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Water Transport'), array('controller' => 'water_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Cruises'); ?></h3>
	<?php if (!empty($cruiseCompany['Cruise'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Cruises Name'); ?></th>
		<th><?php echo __('Number Nights'); ?></th>
		<th><?php echo __('Camarote'); ?></th>
		<th><?php echo __('Service On Board'); ?></th>
		<th><?php echo __('Arrival Time'); ?></th>
		<th><?php echo __('Departure Time'); ?></th>
		<th><?php echo __('Departure Port Id'); ?></th>
		<th><?php echo __('Arrivial Port Id'); ?></th>
		<th><?php echo __('Cruise Company Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($cruiseCompany['Cruise'] as $cruise): ?>
		<tr>
			<td><?php echo $cruise['id']; ?></td>
			<td><?php echo $cruise['cruises_name']; ?></td>
			<td><?php echo $cruise['number_nights']; ?></td>
			<td><?php echo $cruise['camarote']; ?></td>
			<td><?php echo $cruise['service_on_board']; ?></td>
			<td><?php echo $cruise['arrival_time']; ?></td>
			<td><?php echo $cruise['departure_time']; ?></td>
			<td><?php echo $cruise['departure_port_id']; ?></td>
			<td><?php echo $cruise['arrivial_port_id']; ?></td>
			<td><?php echo $cruise['cruise_company_id']; ?></td>
			<td><?php echo $cruise['created']; ?></td>
			<td><?php echo $cruise['modified']; ?></td>
			<td><?php echo $cruise['deleted']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'cruises', 'action' => 'view', $cruise['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'cruises', 'action' => 'edit', $cruise['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'cruises', 'action' => 'delete', $cruise['id']), array(), __('Are you sure you want to delete # %s?', $cruise['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Cruise'), array('controller' => 'cruises', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Water Transports'); ?></h3>
	<?php if (!empty($cruiseCompany['WaterTransport'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Travel Number'); ?></th>
		<th><?php echo __('Cruise Company Id'); ?></th>
		<th><?php echo __('Departure Port Id'); ?></th>
		<th><?php echo __('Arrival Port Id'); ?></th>
		<th><?php echo __('Departure Time'); ?></th>
		<th><?php echo __('Arrival Time'); ?></th>
		<th><?php echo __('Departure Id'); ?></th>
		<th><?php echo __('Cruises Name'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($cruiseCompany['WaterTransport'] as $waterTransport): ?>
		<tr>
			<td><?php echo $waterTransport['id']; ?></td>
			<td><?php echo $waterTransport['travel_number']; ?></td>
			<td><?php echo $waterTransport['cruise_company_id']; ?></td>
			<td><?php echo $waterTransport['departure_port_id']; ?></td>
			<td><?php echo $waterTransport['arrival_port_id']; ?></td>
			<td><?php echo $waterTransport['departure_time']; ?></td>
			<td><?php echo $waterTransport['arrival_time']; ?></td>
			<td><?php echo $waterTransport['departure_id']; ?></td>
			<td><?php echo $waterTransport['cruises_name']; ?></td>
			<td><?php echo $waterTransport['deleted']; ?></td>
			<td><?php echo $waterTransport['created']; ?></td>
			<td><?php echo $waterTransport['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'water_transports', 'action' => 'view', $waterTransport['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'water_transports', 'action' => 'edit', $waterTransport['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'water_transports', 'action' => 'delete', $waterTransport['id']), array(), __('Are you sure you want to delete # %s?', $waterTransport['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Water Transport'), array('controller' => 'water_transports', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

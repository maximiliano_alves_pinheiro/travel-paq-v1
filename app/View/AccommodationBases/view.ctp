<div class="accommodationBases view">
<h2><?php echo __('Accommodation Base'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($accommodationBase['AccommodationBase']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($accommodationBase['AccommodationBase']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($accommodationBase['AccommodationBase']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($accommodationBase['AccommodationBase']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($accommodationBase['AccommodationBase']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Accommodation Base'), array('action' => 'edit', $accommodationBase['AccommodationBase']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Accommodation Base'), array('action' => 'delete', $accommodationBase['AccommodationBase']['id']), array(), __('Are you sure you want to delete # %s?', $accommodationBase['AccommodationBase']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Accommodation Bases'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accommodation Base'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prices'), array('controller' => 'prices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Price'), array('controller' => 'prices', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Prices'); ?></h3>
	<?php if (!empty($accommodationBase['Price'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Currency'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Tax'); ?></th>
		<th><?php echo __('Perception'); ?></th>
		<th><?php echo __('Accommodation Base Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($accommodationBase['Price'] as $price): ?>
		<tr>
			<td><?php echo $price['id']; ?></td>
			<td><?php echo $price['currency']; ?></td>
			<td><?php echo $price['price']; ?></td>
			<td><?php echo $price['tax']; ?></td>
			<td><?php echo $price['perception']; ?></td>
			<td><?php echo $price['accommodation_base_id']; ?></td>
			<td><?php echo $price['deleted']; ?></td>
			<td><?php echo $price['created']; ?></td>
			<td><?php echo $price['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'prices', 'action' => 'view', $price['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'prices', 'action' => 'edit', $price['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'prices', 'action' => 'delete', $price['id']), array(), __('Are you sure you want to delete # %s?', $price['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Price'), array('controller' => 'prices', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

<div class="cruises form">
<?php echo $this->Form->create('Cruise'); ?>
	<fieldset>
		<legend><?php echo __('Add Cruise'); ?></legend>
	<?php
		echo $this->Form->input('cruises_name');
		echo $this->Form->input('number_nights');
		echo $this->Form->input('camarote');
		echo $this->Form->input('service_on_board');
		echo $this->Form->input('arrival_time');
		echo $this->Form->input('departure_time');
		echo $this->Form->input('departure_port_id');
		echo $this->Form->input('arrivial_port_id');
		echo $this->Form->input('cruise_company_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('Place');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Cruises'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Cruise Companies'), array('controller' => 'cruise_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise Company'), array('controller' => 'cruise_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
	</ul>
</div>

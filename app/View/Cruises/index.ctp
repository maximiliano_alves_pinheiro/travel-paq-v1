<div class="cruises index">
	<h2><?php echo __('Cruises'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('cruises_name'); ?></th>
			<th><?php echo $this->Paginator->sort('number_nights'); ?></th>
			<th><?php echo $this->Paginator->sort('camarote'); ?></th>
			<th><?php echo $this->Paginator->sort('service_on_board'); ?></th>
			<th><?php echo $this->Paginator->sort('arrival_time'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_time'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_port_id'); ?></th>
			<th><?php echo $this->Paginator->sort('arrivial_port_id'); ?></th>
			<th><?php echo $this->Paginator->sort('cruise_company_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($cruises as $cruise): ?>
	<tr>
		<td><?php echo h($cruise['Cruise']['id']); ?>&nbsp;</td>
		<td><?php echo h($cruise['Cruise']['cruises_name']); ?>&nbsp;</td>
		<td><?php echo h($cruise['Cruise']['number_nights']); ?>&nbsp;</td>
		<td><?php echo h($cruise['Cruise']['camarote']); ?>&nbsp;</td>
		<td><?php echo h($cruise['Cruise']['service_on_board']); ?>&nbsp;</td>
		<td><?php echo h($cruise['Cruise']['arrival_time']); ?>&nbsp;</td>
		<td><?php echo h($cruise['Cruise']['departure_time']); ?>&nbsp;</td>
		<td><?php echo h($cruise['Cruise']['departure_port_id']); ?>&nbsp;</td>
		<td><?php echo h($cruise['Cruise']['arrivial_port_id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($cruise['CruiseCompany']['name'], array('controller' => 'cruise_companies', 'action' => 'view', $cruise['CruiseCompany']['id'])); ?>
		</td>
		<td><?php echo h($cruise['Cruise']['created']); ?>&nbsp;</td>
		<td><?php echo h($cruise['Cruise']['modified']); ?>&nbsp;</td>
		<td><?php echo h($cruise['Cruise']['deleted']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $cruise['Cruise']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $cruise['Cruise']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $cruise['Cruise']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $cruise['Cruise']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Cruise'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Cruise Companies'), array('controller' => 'cruise_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise Company'), array('controller' => 'cruise_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="cruises view">
<h2><?php echo __('Cruise'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cruises Name'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['cruises_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number Nights'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['number_nights']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Camarote'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['camarote']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Service On Board'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['service_on_board']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Arrival Time'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['arrival_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure Time'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['departure_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure Port Id'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['departure_port_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Arrivial Port Id'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['arrivial_port_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cruise Company'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cruise['CruiseCompany']['name'], array('controller' => 'cruise_companies', 'action' => 'view', $cruise['CruiseCompany']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($cruise['Cruise']['deleted']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cruise'), array('action' => 'edit', $cruise['Cruise']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cruise'), array('action' => 'delete', $cruise['Cruise']['id']), array(), __('Are you sure you want to delete # %s?', $cruise['Cruise']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cruises'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cruise Companies'), array('controller' => 'cruise_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cruise Company'), array('controller' => 'cruise_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Places'); ?></h3>
	<?php if (!empty($cruise['Place'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Country'); ?></th>
		<th><?php echo __('Region'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($cruise['Place'] as $place): ?>
		<tr>
			<td><?php echo $place['id']; ?></td>
			<td><?php echo $place['name']; ?></td>
			<td><?php echo $place['country']; ?></td>
			<td><?php echo $place['region']; ?></td>
			<td><?php echo $place['deleted']; ?></td>
			<td><?php echo $place['created']; ?></td>
			<td><?php echo $place['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'places', 'action' => 'view', $place['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'places', 'action' => 'edit', $place['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'places', 'action' => 'delete', $place['id']), array(), __('Are you sure you want to delete # %s?', $place['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

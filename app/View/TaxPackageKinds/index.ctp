<div class="taxPackageKinds index">
	<h2><?php echo __('Tax Package Kinds'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($taxPackageKinds as $taxPackageKind): ?>
	<tr>
		<td><?php echo h($taxPackageKind['TaxPackageKind']['id']); ?>&nbsp;</td>
		<td><?php echo h($taxPackageKind['TaxPackageKind']['name']); ?>&nbsp;</td>
		<td><?php echo h($taxPackageKind['TaxPackageKind']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($taxPackageKind['TaxPackageKind']['created']); ?>&nbsp;</td>
		<td><?php echo h($taxPackageKind['TaxPackageKind']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $taxPackageKind['TaxPackageKind']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $taxPackageKind['TaxPackageKind']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $taxPackageKind['TaxPackageKind']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $taxPackageKind['TaxPackageKind']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Tax Package Kind'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Package Taxes'), array('controller' => 'package_taxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package Tax'), array('controller' => 'package_taxes', 'action' => 'add')); ?> </li>
	</ul>
</div>

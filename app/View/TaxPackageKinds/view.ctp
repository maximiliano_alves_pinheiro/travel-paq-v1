<div class="taxPackageKinds view">
<h2><?php echo __('Tax Package Kind'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($taxPackageKind['TaxPackageKind']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($taxPackageKind['TaxPackageKind']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($taxPackageKind['TaxPackageKind']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($taxPackageKind['TaxPackageKind']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($taxPackageKind['TaxPackageKind']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tax Package Kind'), array('action' => 'edit', $taxPackageKind['TaxPackageKind']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tax Package Kind'), array('action' => 'delete', $taxPackageKind['TaxPackageKind']['id']), array(), __('Are you sure you want to delete # %s?', $taxPackageKind['TaxPackageKind']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tax Package Kinds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tax Package Kind'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Package Taxes'), array('controller' => 'package_taxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package Tax'), array('controller' => 'package_taxes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Package Taxes'); ?></h3>
	<?php if (!empty($taxPackageKind['PackageTax'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Currency'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Tax Package Kind Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($taxPackageKind['PackageTax'] as $packageTax): ?>
		<tr>
			<td><?php echo $packageTax['id']; ?></td>
			<td><?php echo $packageTax['currency']; ?></td>
			<td><?php echo $packageTax['price']; ?></td>
			<td><?php echo $packageTax['tax_package_kind_id']; ?></td>
			<td><?php echo $packageTax['deleted']; ?></td>
			<td><?php echo $packageTax['created']; ?></td>
			<td><?php echo $packageTax['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'package_taxes', 'action' => 'view', $packageTax['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'package_taxes', 'action' => 'edit', $packageTax['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'package_taxes', 'action' => 'delete', $packageTax['id']), array(), __('Are you sure you want to delete # %s?', $packageTax['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Package Tax'), array('controller' => 'package_taxes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

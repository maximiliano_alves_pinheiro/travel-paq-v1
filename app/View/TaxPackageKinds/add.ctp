<div class="taxPackageKinds form">
<?php echo $this->Form->create('TaxPackageKind'); ?>
	<fieldset>
		<legend><?php echo __('Add Tax Package Kind'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Tax Package Kinds'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Package Taxes'), array('controller' => 'package_taxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package Tax'), array('controller' => 'package_taxes', 'action' => 'add')); ?> </li>
	</ul>
</div>

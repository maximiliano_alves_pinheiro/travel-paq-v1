<div class="accommodations index">
	<h2><?php echo __('Accommodations'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('hotel_service_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($accommodations as $accommodation): ?>
	<tr>
		<td><?php echo h($accommodation['Accommodation']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($accommodation['HotelService']['name'], array('controller' => 'hotel_services', 'action' => 'view', $accommodation['HotelService']['id'])); ?>
		</td>
		<td><?php echo h($accommodation['Accommodation']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($accommodation['Accommodation']['created']); ?>&nbsp;</td>
		<td><?php echo h($accommodation['Accommodation']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $accommodation['Accommodation']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $accommodation['Accommodation']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $accommodation['Accommodation']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $accommodation['Accommodation']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Accommodation'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Hotel Services'), array('controller' => 'hotel_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Hotel Service'), array('controller' => 'hotel_services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Hotels'), array('controller' => 'hotels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Hotel'), array('controller' => 'hotels', 'action' => 'add')); ?> </li>
	</ul>
</div>

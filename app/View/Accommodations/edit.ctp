<div class="accommodations form">
<?php echo $this->Form->create('Accommodation'); ?>
	<fieldset>
		<legend><?php echo __('Edit Accommodation'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('hotel_service_id');
		echo $this->Form->input('deleted');
		echo $this->Form->input('Hotel');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Accommodation.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Accommodation.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Accommodations'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Hotel Services'), array('controller' => 'hotel_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Hotel Service'), array('controller' => 'hotel_services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Hotels'), array('controller' => 'hotels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Hotel'), array('controller' => 'hotels', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="accommodations view">
<h2><?php echo __('Accommodation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($accommodation['Accommodation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hotel Service'); ?></dt>
		<dd>
			<?php echo $this->Html->link($accommodation['HotelService']['name'], array('controller' => 'hotel_services', 'action' => 'view', $accommodation['HotelService']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($accommodation['Accommodation']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($accommodation['Accommodation']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($accommodation['Accommodation']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Accommodation'), array('action' => 'edit', $accommodation['Accommodation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Accommodation'), array('action' => 'delete', $accommodation['Accommodation']['id']), array(), __('Are you sure you want to delete # %s?', $accommodation['Accommodation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Accommodations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accommodation'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Hotel Services'), array('controller' => 'hotel_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Hotel Service'), array('controller' => 'hotel_services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Hotels'), array('controller' => 'hotels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Hotel'), array('controller' => 'hotels', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Hotels'); ?></h3>
	<?php if (!empty($accommodation['Hotel'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Maxi Children Age'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($accommodation['Hotel'] as $hotel): ?>
		<tr>
			<td><?php echo $hotel['id']; ?></td>
			<td><?php echo $hotel['name']; ?></td>
			<td><?php echo $hotel['maxi_children_age']; ?></td>
			<td><?php echo $hotel['deleted']; ?></td>
			<td><?php echo $hotel['created']; ?></td>
			<td><?php echo $hotel['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'hotels', 'action' => 'view', $hotel['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'hotels', 'action' => 'edit', $hotel['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'hotels', 'action' => 'delete', $hotel['id']), array(), __('Are you sure you want to delete # %s?', $hotel['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Hotel'), array('controller' => 'hotels', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

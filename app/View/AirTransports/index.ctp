<div class="airTransports index">
	<h2><?php echo __('Air Transports'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('flight_number'); ?></th>
			<th><?php echo $this->Paginator->sort('order_number'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_time'); ?></th>
			<th><?php echo $this->Paginator->sort('arrival_time'); ?></th>
			<th><?php echo $this->Paginator->sort('airline_id'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_airport_id'); ?></th>
			<th><?php echo $this->Paginator->sort('arrival_airport_id'); ?></th>
			<th><?php echo $this->Paginator->sort('departure_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($airTransports as $airTransport): ?>
	<tr>
		<td><?php echo h($airTransport['AirTransport']['id']); ?>&nbsp;</td>
		<td><?php echo h($airTransport['AirTransport']['flight_number']); ?>&nbsp;</td>
		<td><?php echo h($airTransport['AirTransport']['order_number']); ?>&nbsp;</td>
		<td><?php echo h($airTransport['AirTransport']['departure_time']); ?>&nbsp;</td>
		<td><?php echo h($airTransport['AirTransport']['arrival_time']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($airTransport['Airline']['name'], array('controller' => 'airlines', 'action' => 'view', $airTransport['Airline']['id'])); ?>
		</td>
		<td><?php echo h($airTransport['AirTransport']['departure_airport_id']); ?>&nbsp;</td>
		<td><?php echo h($airTransport['AirTransport']['arrival_airport_id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($airTransport['Departure']['id'], array('controller' => 'departures', 'action' => 'view', $airTransport['Departure']['id'])); ?>
		</td>
		<td><?php echo h($airTransport['AirTransport']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($airTransport['AirTransport']['created']); ?>&nbsp;</td>
		<td><?php echo h($airTransport['AirTransport']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $airTransport['AirTransport']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $airTransport['AirTransport']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $airTransport['AirTransport']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $airTransport['AirTransport']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Air Transport'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Airlines'), array('controller' => 'airlines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Airline'), array('controller' => 'airlines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departures'), array('controller' => 'departures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Departure'), array('controller' => 'departures', 'action' => 'add')); ?> </li>
	</ul>
</div>

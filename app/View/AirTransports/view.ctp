<div class="airTransports view">
<h2><?php echo __('Air Transport'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($airTransport['AirTransport']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Flight Number'); ?></dt>
		<dd>
			<?php echo h($airTransport['AirTransport']['flight_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order Number'); ?></dt>
		<dd>
			<?php echo h($airTransport['AirTransport']['order_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure Time'); ?></dt>
		<dd>
			<?php echo h($airTransport['AirTransport']['departure_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Arrival Time'); ?></dt>
		<dd>
			<?php echo h($airTransport['AirTransport']['arrival_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Airline'); ?></dt>
		<dd>
			<?php echo $this->Html->link($airTransport['Airline']['name'], array('controller' => 'airlines', 'action' => 'view', $airTransport['Airline']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure Airport Id'); ?></dt>
		<dd>
			<?php echo h($airTransport['AirTransport']['departure_airport_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Arrival Airport Id'); ?></dt>
		<dd>
			<?php echo h($airTransport['AirTransport']['arrival_airport_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Departure'); ?></dt>
		<dd>
			<?php echo $this->Html->link($airTransport['Departure']['id'], array('controller' => 'departures', 'action' => 'view', $airTransport['Departure']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($airTransport['AirTransport']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($airTransport['AirTransport']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($airTransport['AirTransport']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Air Transport'), array('action' => 'edit', $airTransport['AirTransport']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Air Transport'), array('action' => 'delete', $airTransport['AirTransport']['id']), array(), __('Are you sure you want to delete # %s?', $airTransport['AirTransport']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Air Transports'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Air Transport'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Airlines'), array('controller' => 'airlines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Airline'), array('controller' => 'airlines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departures'), array('controller' => 'departures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Departure'), array('controller' => 'departures', 'action' => 'add')); ?> </li>
	</ul>
</div>

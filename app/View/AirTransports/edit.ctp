<div class="airTransports form">
<?php echo $this->Form->create('AirTransport'); ?>
	<fieldset>
		<legend><?php echo __('Edit Air Transport'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('flight_number');
		echo $this->Form->input('order_number');
		echo $this->Form->input('departure_time');
		echo $this->Form->input('arrival_time');
		echo $this->Form->input('airline_id');
		echo $this->Form->input('departure_airport_id');
		echo $this->Form->input('arrival_airport_id');
		echo $this->Form->input('departure_id');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AirTransport.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('AirTransport.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Air Transports'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Airlines'), array('controller' => 'airlines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Airline'), array('controller' => 'airlines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departures'), array('controller' => 'departures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Departure'), array('controller' => 'departures', 'action' => 'add')); ?> </li>
	</ul>
</div>

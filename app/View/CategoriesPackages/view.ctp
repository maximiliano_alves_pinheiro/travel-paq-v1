<div class="categoriesPackages view">
<h2><?php echo __('Categories Package'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($categoriesPackage['CategoriesPackage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($categoriesPackage['Category']['name'], array('controller' => 'categories', 'action' => 'view', $categoriesPackage['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Package'); ?></dt>
		<dd>
			<?php echo $this->Html->link($categoriesPackage['Package']['title'], array('controller' => 'packages', 'action' => 'view', $categoriesPackage['Package']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($categoriesPackage['CategoriesPackage']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($categoriesPackage['CategoriesPackage']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($categoriesPackage['CategoriesPackage']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Categories Package'), array('action' => 'edit', $categoriesPackage['CategoriesPackage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Categories Package'), array('action' => 'delete', $categoriesPackage['CategoriesPackage']['id']), array(), __('Are you sure you want to delete # %s?', $categoriesPackage['CategoriesPackage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories Packages'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Categories Package'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages'), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package'), array('controller' => 'packages', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="busCompanies form">
<?php echo $this->Form->create('BusCompany'); ?>
	<fieldset>
		<legend><?php echo __('Edit Bus Company'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('BusCompany.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('BusCompany.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Bus Companies'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Ground Transports'), array('controller' => 'ground_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ground Transport'), array('controller' => 'ground_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>

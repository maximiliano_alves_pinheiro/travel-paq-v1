<div class="busCompanies view">
<h2><?php echo __('Bus Company'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($busCompany['BusCompany']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($busCompany['BusCompany']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($busCompany['BusCompany']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($busCompany['BusCompany']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($busCompany['BusCompany']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bus Company'), array('action' => 'edit', $busCompany['BusCompany']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Bus Company'), array('action' => 'delete', $busCompany['BusCompany']['id']), array(), __('Are you sure you want to delete # %s?', $busCompany['BusCompany']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bus Companies'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bus Company'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ground Transports'), array('controller' => 'ground_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ground Transport'), array('controller' => 'ground_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Ground Transports'); ?></h3>
	<?php if (!empty($busCompany['GroundTransport'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Travel Number'); ?></th>
		<th><?php echo __('Arrival Time'); ?></th>
		<th><?php echo __('Departure Time'); ?></th>
		<th><?php echo __('Departure Terminal Id'); ?></th>
		<th><?php echo __('Arrival Terminal Id'); ?></th>
		<th><?php echo __('Bus Company Id'); ?></th>
		<th><?php echo __('Departure Id'); ?></th>
		<th><?php echo __('Deleted'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($busCompany['GroundTransport'] as $groundTransport): ?>
		<tr>
			<td><?php echo $groundTransport['id']; ?></td>
			<td><?php echo $groundTransport['travel_number']; ?></td>
			<td><?php echo $groundTransport['arrival_time']; ?></td>
			<td><?php echo $groundTransport['departure_time']; ?></td>
			<td><?php echo $groundTransport['departure_terminal_id']; ?></td>
			<td><?php echo $groundTransport['arrival_terminal_id']; ?></td>
			<td><?php echo $groundTransport['bus_company_id']; ?></td>
			<td><?php echo $groundTransport['departure_id']; ?></td>
			<td><?php echo $groundTransport['deleted']; ?></td>
			<td><?php echo $groundTransport['created']; ?></td>
			<td><?php echo $groundTransport['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ground_transports', 'action' => 'view', $groundTransport['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ground_transports', 'action' => 'edit', $groundTransport['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ground_transports', 'action' => 'delete', $groundTransport['id']), array(), __('Are you sure you want to delete # %s?', $groundTransport['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ground Transport'), array('controller' => 'ground_transports', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

<div class="busCompanies index">
	<h2><?php echo __('Bus Companies'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('deleted'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($busCompanies as $busCompany): ?>
	<tr>
		<td><?php echo h($busCompany['BusCompany']['id']); ?>&nbsp;</td>
		<td><?php echo h($busCompany['BusCompany']['name']); ?>&nbsp;</td>
		<td><?php echo h($busCompany['BusCompany']['deleted']); ?>&nbsp;</td>
		<td><?php echo h($busCompany['BusCompany']['created']); ?>&nbsp;</td>
		<td><?php echo h($busCompany['BusCompany']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $busCompany['BusCompany']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $busCompany['BusCompany']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $busCompany['BusCompany']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $busCompany['BusCompany']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Bus Company'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Ground Transports'), array('controller' => 'ground_transports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ground Transport'), array('controller' => 'ground_transports', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="prices form">
<?php echo $this->Form->create('Price'); ?>
	<fieldset>
		<legend><?php echo __('Edit Price'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('currency');
		echo $this->Form->input('price');
		echo $this->Form->input('tax');
		echo $this->Form->input('perception');
		echo $this->Form->input('accommodation_base_id');
		echo $this->Form->input('deleted');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Price.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Price.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Prices'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Accommodation Bases'), array('controller' => 'accommodation_bases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accommodation Base'), array('controller' => 'accommodation_bases', 'action' => 'add')); ?> </li>
	</ul>
</div>

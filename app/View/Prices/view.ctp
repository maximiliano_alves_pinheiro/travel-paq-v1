<div class="prices view">
<h2><?php echo __('Price'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($price['Price']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Currency'); ?></dt>
		<dd>
			<?php echo h($price['Price']['currency']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($price['Price']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tax'); ?></dt>
		<dd>
			<?php echo h($price['Price']['tax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Perception'); ?></dt>
		<dd>
			<?php echo h($price['Price']['perception']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Accommodation Base'); ?></dt>
		<dd>
			<?php echo $this->Html->link($price['AccommodationBase']['name'], array('controller' => 'accommodation_bases', 'action' => 'view', $price['AccommodationBase']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deleted'); ?></dt>
		<dd>
			<?php echo h($price['Price']['deleted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($price['Price']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($price['Price']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Price'), array('action' => 'edit', $price['Price']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Price'), array('action' => 'delete', $price['Price']['id']), array(), __('Are you sure you want to delete # %s?', $price['Price']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Prices'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Price'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accommodation Bases'), array('controller' => 'accommodation_bases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accommodation Base'), array('controller' => 'accommodation_bases', 'action' => 'add')); ?> </li>
	</ul>
</div>

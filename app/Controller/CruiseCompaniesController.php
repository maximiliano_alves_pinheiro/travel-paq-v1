<?php
App::uses('AppController', 'Controller');
/**
 * CruiseCompanies Controller
 *
 * @property CruiseCompany $CruiseCompany
 * @property PaginatorComponent $Paginator
 */
class CruiseCompaniesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CruiseCompany->recursive = 0;
		$this->set('cruiseCompanies', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CruiseCompany->exists($id)) {
			throw new NotFoundException(__('Invalid cruise company'));
		}
		$options = array('conditions' => array('CruiseCompany.' . $this->CruiseCompany->primaryKey => $id));
		$this->set('cruiseCompany', $this->CruiseCompany->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CruiseCompany->create();
			if ($this->CruiseCompany->save($this->request->data)) {
				$this->Session->setFlash(__('The cruise company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cruise company could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CruiseCompany->exists($id)) {
			throw new NotFoundException(__('Invalid cruise company'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CruiseCompany->save($this->request->data)) {
				$this->Session->setFlash(__('The cruise company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cruise company could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CruiseCompany.' . $this->CruiseCompany->primaryKey => $id));
			$this->request->data = $this->CruiseCompany->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CruiseCompany->id = $id;
		if (!$this->CruiseCompany->exists()) {
			throw new NotFoundException(__('Invalid cruise company'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CruiseCompany->delete()) {
			$this->Session->setFlash(__('The cruise company has been deleted.'));
		} else {
			$this->Session->setFlash(__('The cruise company could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

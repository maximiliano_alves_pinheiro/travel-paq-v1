<?php
App::uses('AppController', 'Controller');
/**
 * PackagesPlaces Controller
 *
 * @property PackagesPlace $PackagesPlace
 * @property PaginatorComponent $Paginator
 */
class PackagesPlacesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->PackagesPlace->recursive = 0;
		$this->set('packagesPlaces', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->PackagesPlace->exists($id)) {
			throw new NotFoundException(__('Invalid packages place'));
		}
		$options = array('conditions' => array('PackagesPlace.' . $this->PackagesPlace->primaryKey => $id));
		$this->set('packagesPlace', $this->PackagesPlace->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->PackagesPlace->create();
			if ($this->PackagesPlace->save($this->request->data)) {
				$this->Session->setFlash(__('The packages place has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The packages place could not be saved. Please, try again.'));
			}
		}
		$places = $this->PackagesPlace->Place->find('list');
		$packages = $this->PackagesPlace->Package->find('list');
		$this->set(compact('places', 'packages'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->PackagesPlace->exists($id)) {
			throw new NotFoundException(__('Invalid packages place'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PackagesPlace->save($this->request->data)) {
				$this->Session->setFlash(__('The packages place has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The packages place could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PackagesPlace.' . $this->PackagesPlace->primaryKey => $id));
			$this->request->data = $this->PackagesPlace->find('first', $options);
		}
		$places = $this->PackagesPlace->Place->find('list');
		$packages = $this->PackagesPlace->Package->find('list');
		$this->set(compact('places', 'packages'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->PackagesPlace->id = $id;
		if (!$this->PackagesPlace->exists()) {
			throw new NotFoundException(__('Invalid packages place'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PackagesPlace->delete()) {
			$this->Session->setFlash(__('The packages place has been deleted.'));
		} else {
			$this->Session->setFlash(__('The packages place could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

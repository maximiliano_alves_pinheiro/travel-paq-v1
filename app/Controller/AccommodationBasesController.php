<?php
App::uses('AppController', 'Controller');
/**
 * AccommodationBases Controller
 *
 * @property AccommodationBase $AccommodationBase
 * @property PaginatorComponent $Paginator
 */
class AccommodationBasesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AccommodationBase->recursive = 0;
		$this->set('accommodationBases', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AccommodationBase->exists($id)) {
			throw new NotFoundException(__('Invalid accommodation base'));
		}
		$options = array('conditions' => array('AccommodationBase.' . $this->AccommodationBase->primaryKey => $id));
		$this->set('accommodationBase', $this->AccommodationBase->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AccommodationBase->create();
			if ($this->AccommodationBase->save($this->request->data)) {
				$this->Session->setFlash(__('The accommodation base has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The accommodation base could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AccommodationBase->exists($id)) {
			throw new NotFoundException(__('Invalid accommodation base'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AccommodationBase->save($this->request->data)) {
				$this->Session->setFlash(__('The accommodation base has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The accommodation base could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AccommodationBase.' . $this->AccommodationBase->primaryKey => $id));
			$this->request->data = $this->AccommodationBase->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AccommodationBase->id = $id;
		if (!$this->AccommodationBase->exists()) {
			throw new NotFoundException(__('Invalid accommodation base'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->AccommodationBase->delete()) {
			$this->Session->setFlash(__('The accommodation base has been deleted.'));
		} else {
			$this->Session->setFlash(__('The accommodation base could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

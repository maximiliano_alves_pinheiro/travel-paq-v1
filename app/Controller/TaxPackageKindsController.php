<?php
App::uses('AppController', 'Controller');
/**
 * TaxPackageKinds Controller
 *
 * @property TaxPackageKind $TaxPackageKind
 * @property PaginatorComponent $Paginator
 */
class TaxPackageKindsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TaxPackageKind->recursive = 0;
		$this->set('taxPackageKinds', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TaxPackageKind->exists($id)) {
			throw new NotFoundException(__('Invalid tax package kind'));
		}
		$options = array('conditions' => array('TaxPackageKind.' . $this->TaxPackageKind->primaryKey => $id));
		$this->set('taxPackageKind', $this->TaxPackageKind->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TaxPackageKind->create();
			if ($this->TaxPackageKind->save($this->request->data)) {
				$this->Session->setFlash(__('The tax package kind has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tax package kind could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TaxPackageKind->exists($id)) {
			throw new NotFoundException(__('Invalid tax package kind'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TaxPackageKind->save($this->request->data)) {
				$this->Session->setFlash(__('The tax package kind has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tax package kind could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TaxPackageKind.' . $this->TaxPackageKind->primaryKey => $id));
			$this->request->data = $this->TaxPackageKind->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TaxPackageKind->id = $id;
		if (!$this->TaxPackageKind->exists()) {
			throw new NotFoundException(__('Invalid tax package kind'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TaxPackageKind->delete()) {
			$this->Session->setFlash(__('The tax package kind has been deleted.'));
		} else {
			$this->Session->setFlash(__('The tax package kind could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

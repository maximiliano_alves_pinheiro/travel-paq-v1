<?php
App::uses('AppController', 'Controller');
/**
 * CategoriesPackages Controller
 *
 * @property CategoriesPackage $CategoriesPackage
 * @property PaginatorComponent $Paginator
 */
class CategoriesPackagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CategoriesPackage->recursive = 0;
		$this->set('categoriesPackages', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CategoriesPackage->exists($id)) {
			throw new NotFoundException(__('Invalid categories package'));
		}
		$options = array('conditions' => array('CategoriesPackage.' . $this->CategoriesPackage->primaryKey => $id));
		$this->set('categoriesPackage', $this->CategoriesPackage->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CategoriesPackage->create();
			if ($this->CategoriesPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The categories package has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The categories package could not be saved. Please, try again.'));
			}
		}
		$categories = $this->CategoriesPackage->Category->find('list');
		$packages = $this->CategoriesPackage->Package->find('list');
		$this->set(compact('categories', 'packages'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CategoriesPackage->exists($id)) {
			throw new NotFoundException(__('Invalid categories package'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CategoriesPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The categories package has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The categories package could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CategoriesPackage.' . $this->CategoriesPackage->primaryKey => $id));
			$this->request->data = $this->CategoriesPackage->find('first', $options);
		}
		$categories = $this->CategoriesPackage->Category->find('list');
		$packages = $this->CategoriesPackage->Package->find('list');
		$this->set(compact('categories', 'packages'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CategoriesPackage->id = $id;
		if (!$this->CategoriesPackage->exists()) {
			throw new NotFoundException(__('Invalid categories package'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CategoriesPackage->delete()) {
			$this->Session->setFlash(__('The categories package has been deleted.'));
		} else {
			$this->Session->setFlash(__('The categories package could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

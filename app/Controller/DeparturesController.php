<?php
App::uses('AppController', 'Controller');
/**
 * Departures Controller
 *
 * @property Departure $Departure
 * @property PaginatorComponent $Paginator
 */
class DeparturesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Departure->recursive = 0;
		$this->set('departures', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Departure->exists($id)) {
			throw new NotFoundException(__('Invalid departure'));
		}
		$options = array('conditions' => array('Departure.' . $this->Departure->primaryKey => $id));
		$this->set('departure', $this->Departure->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Departure->create();
			if ($this->Departure->save($this->request->data)) {
				$this->Session->setFlash(__('The departure has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The departure could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Departure->exists($id)) {
			throw new NotFoundException(__('Invalid departure'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Departure->save($this->request->data)) {
				$this->Session->setFlash(__('The departure has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The departure could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Departure.' . $this->Departure->primaryKey => $id));
			$this->request->data = $this->Departure->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Departure->id = $id;
		if (!$this->Departure->exists()) {
			throw new NotFoundException(__('Invalid departure'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Departure->delete()) {
			$this->Session->setFlash(__('The departure has been deleted.'));
		} else {
			$this->Session->setFlash(__('The departure could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

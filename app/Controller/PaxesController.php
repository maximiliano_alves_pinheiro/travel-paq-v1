<?php
App::uses('AppController', 'Controller');
/**
 * Places Controller
 *
 * @property Place $Place
 * @property RequestHandlerComponent $RequestHandler
 */
class PaxesController extends AppController {

/**
 * Components
 *
 * @var array
 */ 
	public $components = array('RequestHandler');
	public $uses = array('Pax');
	public $name = "paxes";

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$paxes = $this->Pax->find('all', array('recursive' => 0, 'conditions' => array('Pax.deleted' => 0)));
		$this->set(array(
			'paxes' => $paxes,
			'_serialize' => array('paxes')
		));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$place = $this->Place->findById($id);
		$this->set(array(
			'place' => $place,
			'_serialize' => array('place')
		));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoRender = false;
		echo json_encode($this->request->data);
		if ($this->request->is('post')) {
			$this->Pax->create();
			if ($this->Pax->save($this->request->data)) {
				$message = array("status"=> "OK");
				echo json_encode($message);
			} else {
				$message = array("status"=> "Error","message"=> $this->Pax);
				echo json_encode($message);
			}
		}
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if ($this->request->is(array('post', 'put'))) {
			$this->Place->id = $id;
			if ($this->Place->saveAll($this->request->data)) {
				$message = 'Guardado.';
			} else {
				$message = 'Error.';
			}
			$this->set(array(
				'message' => $message,
				'_serialize' => array('message')
			));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Place->id = $id;
		if ($this->Place->saveField('deleted', 1)) {
			$message = 'Borrado.';
		} else {
			$message = 'Error.';
		}
		$this->set(array(
			'message' => $message,
			'_serialize' => array('message')
		));
	}


	public function isAuthorized($user) {
	    // All registered users can add posts
	    if ($this->action === 'add') {
	        return true;
	    }

	    // The owner of a post can edit and delete it
	    if (in_array($this->action, array('edit', 'delete'))) {
	        $postId = $this->request->params['pass'][0];
	        if ($this->Post->isOwnedBy($postId, $user['id'])) {
	        return true;
	        }
	    }

	    return parent::isAuthorized($user);
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * ServiceKinds Controller
 *
 * @property ServiceKind $ServiceKind
 * @property PaginatorComponent $Paginator
 */
class ServiceKindsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ServiceKind->recursive = 0;
		$this->set('serviceKinds', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ServiceKind->exists($id)) {
			throw new NotFoundException(__('Invalid service kind'));
		}
		$options = array('conditions' => array('ServiceKind.' . $this->ServiceKind->primaryKey => $id));
		$this->set('serviceKind', $this->ServiceKind->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ServiceKind->create();
			if ($this->ServiceKind->save($this->request->data)) {
				$this->Session->setFlash(__('The service kind has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The service kind could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ServiceKind->exists($id)) {
			throw new NotFoundException(__('Invalid service kind'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ServiceKind->save($this->request->data)) {
				$this->Session->setFlash(__('The service kind has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The service kind could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ServiceKind.' . $this->ServiceKind->primaryKey => $id));
			$this->request->data = $this->ServiceKind->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ServiceKind->id = $id;
		if (!$this->ServiceKind->exists()) {
			throw new NotFoundException(__('Invalid service kind'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ServiceKind->delete()) {
			$this->Session->setFlash(__('The service kind has been deleted.'));
		} else {
			$this->Session->setFlash(__('The service kind could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

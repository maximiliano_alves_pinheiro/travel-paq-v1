<?php
App::uses('AppController', 'Controller');
/**
 * Accommodations Controller
 *
 * @property Accommodation $Accommodation
 * @property PaginatorComponent $Paginator
 */
class AccommodationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Accommodation->recursive = 0;
		$this->set('accommodations', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Accommodation->exists($id)) {
			throw new NotFoundException(__('Invalid accommodation'));
		}
		$options = array('conditions' => array('Accommodation.' . $this->Accommodation->primaryKey => $id));
		$this->set('accommodation', $this->Accommodation->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Accommodation->create();
			if ($this->Accommodation->save($this->request->data)) {
				$this->Session->setFlash(__('The accommodation has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The accommodation could not be saved. Please, try again.'));
			}
		}
		$hotelServices = $this->Accommodation->HotelService->find('list');
		$hotels = $this->Accommodation->Hotel->find('list');
		$this->set(compact('hotelServices', 'hotels'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Accommodation->exists($id)) {
			throw new NotFoundException(__('Invalid accommodation'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Accommodation->save($this->request->data)) {
				$this->Session->setFlash(__('The accommodation has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The accommodation could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Accommodation.' . $this->Accommodation->primaryKey => $id));
			$this->request->data = $this->Accommodation->find('first', $options);
		}
		$hotelServices = $this->Accommodation->HotelService->find('list');
		$hotels = $this->Accommodation->Hotel->find('list');
		$this->set(compact('hotelServices', 'hotels'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Accommodation->id = $id;
		if (!$this->Accommodation->exists()) {
			throw new NotFoundException(__('Invalid accommodation'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Accommodation->delete()) {
			$this->Session->setFlash(__('The accommodation has been deleted.'));
		} else {
			$this->Session->setFlash(__('The accommodation could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * BusCompanies Controller
 *
 * @property BusCompany $BusCompany
 * @property PaginatorComponent $Paginator
 */
class BusCompaniesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->BusCompany->recursive = 0;
		$this->set('busCompanies', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->BusCompany->exists($id)) {
			throw new NotFoundException(__('Invalid bus company'));
		}
		$options = array('conditions' => array('BusCompany.' . $this->BusCompany->primaryKey => $id));
		$this->set('busCompany', $this->BusCompany->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->BusCompany->create();
			if ($this->BusCompany->save($this->request->data)) {
				$this->Session->setFlash(__('The bus company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bus company could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->BusCompany->exists($id)) {
			throw new NotFoundException(__('Invalid bus company'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BusCompany->save($this->request->data)) {
				$this->Session->setFlash(__('The bus company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bus company could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('BusCompany.' . $this->BusCompany->primaryKey => $id));
			$this->request->data = $this->BusCompany->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->BusCompany->id = $id;
		if (!$this->BusCompany->exists()) {
			throw new NotFoundException(__('Invalid bus company'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->BusCompany->delete()) {
			$this->Session->setFlash(__('The bus company has been deleted.'));
		} else {
			$this->Session->setFlash(__('The bus company could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

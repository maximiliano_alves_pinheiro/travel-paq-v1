<?php
App::uses('AppController', 'Controller');
/**
 * Packages Controller
 *
 * @property Package $Package
 * @property RequestHandlerComponent $RequestHandler
 */
class PackagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('RequestHandler');
	public $uses = array('Package', 'Category', 'AccommodationBase', 'HotelService', 'ServiceKind', 'Airline', 'BusCompany', 'CruiseCompany');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$packages = $this->Package->find('all', array('recursive' => 0, 'conditions' => array('Package.deleted' => 0)));
		$this->set(array(
			'packages' => $packages,
			'_serialize' => array('packages')
		));	
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$packages = $this->Package->findById($id);
		$this->set(array(
			'packages' => $packages,
			'_serialize' => array('packages')
		));	
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$this->Package->create();
			if ($this->Package->saveAll($this->request->data)) {
				$message = 'Guardado.';
			} else {
				$message = 'Error.';
			}
			$this->set(array(
				'message' => $message,
				'_serialize' => array('message')
			));
		} else {
            $categories = $this->Category->find('list');
            $accommodationBases = $this->AccommodationBase->find('all');
            $hotelServices = $this->HotelService->find('all');
            $serviceKinds = $this->ServiceKind->find('all');
            $airlines = $this->Airline->find('all');
            $busCompanies = $this->BusCompany->find('all');
            $cruiseCompanies = $this->CruiseCompany->find('all');
            echo json_encode(array(
                'categories' => $categories,
                'accommodationBases' => $accommodationBases,
                'hotelServices' => $hotelServices,
                'serviceKinds' => $serviceKinds,
                'airlines' => $airlines,
                'busCompanies' => $busCompanies,
                'cruiseCompanies' => $cruiseCompanies
            ));
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoRender = false;
		if ($this->request->is(array('post', 'put'))) {
			$this->Package->id = $id;
			if ($this->Package->saveAll($this->request->data)) {
				$message = 'Guardado.';
			} else {
				$message = 'Error.';
			}
			$this->set(array(
				'message' => $message,
				'_serialize' => array('message')
			));
		} else {
            $categories = $this->Category->find('all');
            $accommodationBases = $this->AccommodationBase->find('all');
            $hotelServices = $this->HotelService->find('all');
            $serviceKinds = $this->ServiceKind->find('all');
            $airlines = $this->Airline->find('all');
            $busCompanies = $this->BusCompany->find('all');
            $cruiseCompanies = $this->CruiseCompany->find('all');
            echo json_encode(array(
                'categories' => $categories,
                'accommodationBases' => $accommodationBases,
                'hotelServices' => $hotelServices,
                'serviceKinds' => $serviceKinds,
                'airlines' => $airlines,
                'busCompanies' => $busCompanies,
                'cruiseCompanies' => $cruiseCompanies
            ));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Package->id = $id;
		if ($this->Package->saveField('deleted', 1)) {
			$message = 'Guardado.';
		} else {
			$message = 'Error.';
		}
		$this->set(array(
			'message' => $message,
			'_serialize' => array('message')
		));
	}
}

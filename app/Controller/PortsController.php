<?php
App::uses('AppController', 'Controller');
/**
 * Ports Controller
 *
 * @property Port $Port
 * @property PaginatorComponent $Paginator
 */
class PortsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Port->recursive = 0;
		$this->set('ports', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Port->exists($id)) {
			throw new NotFoundException(__('Invalid port'));
		}
		$options = array('conditions' => array('Port.' . $this->Port->primaryKey => $id));
		$this->set('port', $this->Port->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Port->create();
			if ($this->Port->save($this->request->data)) {
				$this->Session->setFlash(__('The port has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The port could not be saved. Please, try again.'));
			}
		}
		$places = $this->Port->Place->find('list');
		$this->set(compact('places'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Port->exists($id)) {
			throw new NotFoundException(__('Invalid port'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Port->save($this->request->data)) {
				$this->Session->setFlash(__('The port has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The port could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Port.' . $this->Port->primaryKey => $id));
			$this->request->data = $this->Port->find('first', $options);
		}
		$places = $this->Port->Place->find('list');
		$this->set(compact('places'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Port->id = $id;
		if (!$this->Port->exists()) {
			throw new NotFoundException(__('Invalid port'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Port->delete()) {
			$this->Session->setFlash(__('The port has been deleted.'));
		} else {
			$this->Session->setFlash(__('The port could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

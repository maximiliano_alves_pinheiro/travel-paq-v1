<?php
App::uses('AppController', 'Controller');
/**
 * Airlines Controller
 *
 * @property Airline $Airline
 * @property PaginatorComponent $Paginator
 */
class AirlinesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Airline->recursive = 0;
		$this->set('airlines', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Airline->exists($id)) {
			throw new NotFoundException(__('Invalid airline'));
		}
		$options = array('conditions' => array('Airline.' . $this->Airline->primaryKey => $id));
		$this->set('airline', $this->Airline->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Airline->create();
			if ($this->Airline->save($this->request->data)) {
				$this->Session->setFlash(__('The airline has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The airline could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Airline->exists($id)) {
			throw new NotFoundException(__('Invalid airline'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Airline->save($this->request->data)) {
				$this->Session->setFlash(__('The airline has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The airline could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Airline.' . $this->Airline->primaryKey => $id));
			$this->request->data = $this->Airline->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Airline->id = $id;
		if (!$this->Airline->exists()) {
			throw new NotFoundException(__('Invalid airline'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Airline->delete()) {
			$this->Session->setFlash(__('The airline has been deleted.'));
		} else {
			$this->Session->setFlash(__('The airline could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

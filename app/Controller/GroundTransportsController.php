<?php
App::uses('AppController', 'Controller');
/**
 * GroundTransports Controller
 *
 * @property GroundTransport $GroundTransport
 * @property PaginatorComponent $Paginator
 */
class GroundTransportsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->GroundTransport->recursive = 0;
		$this->set('groundTransports', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->GroundTransport->exists($id)) {
			throw new NotFoundException(__('Invalid ground transport'));
		}
		$options = array('conditions' => array('GroundTransport.' . $this->GroundTransport->primaryKey => $id));
		$this->set('groundTransport', $this->GroundTransport->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->GroundTransport->create();
			if ($this->GroundTransport->save($this->request->data)) {
				$this->Session->setFlash(__('The ground transport has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ground transport could not be saved. Please, try again.'));
			}
		}
		$busCompanies = $this->GroundTransport->BusCompany->find('list');
		$departures = $this->GroundTransport->Departure->find('list');
		$this->set(compact('busCompanies', 'departures'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->GroundTransport->exists($id)) {
			throw new NotFoundException(__('Invalid ground transport'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->GroundTransport->save($this->request->data)) {
				$this->Session->setFlash(__('The ground transport has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ground transport could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('GroundTransport.' . $this->GroundTransport->primaryKey => $id));
			$this->request->data = $this->GroundTransport->find('first', $options);
		}
		$busCompanies = $this->GroundTransport->BusCompany->find('list');
		$departures = $this->GroundTransport->Departure->find('list');
		$this->set(compact('busCompanies', 'departures'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->GroundTransport->id = $id;
		if (!$this->GroundTransport->exists()) {
			throw new NotFoundException(__('Invalid ground transport'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->GroundTransport->delete()) {
			$this->Session->setFlash(__('The ground transport has been deleted.'));
		} else {
			$this->Session->setFlash(__('The ground transport could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

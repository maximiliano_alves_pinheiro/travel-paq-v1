<?php
App::uses('AppController', 'Controller');
/**
 * Places Controller
 *
 * @property Place $Place
 * @property RequestHandlerComponent $RequestHandler
 */
class PlacesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('RequestHandler');
	public $uses = array('Place');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$places = $this->Place->find('all', array('recursive' => 0, 'conditions' => array('Place.deleted' => 0)));
		$this->set(array(
			'places' => $places,
			'_serialize' => array('places')
		));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$place = $this->Place->findById($id);
		$this->set(array(
			'place' => $place,
			'_serialize' => array('place')
		));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->request->data['Place']['user_id'] = $this->Auth->user('id');
		if ($this->request->is('post')) {
			$this->Place->create();
			if ($this->Place->saveAll($this->request->data)) {
				$message = 'Guardado';
			} else {
				$message = 'Error.';
			}
			$this->set(array(
				'message' => $message,
				'_serialize' => array('message')
			));
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if ($this->request->is(array('post', 'put'))) {
			$this->Place->id = $id;
			if ($this->Place->saveAll($this->request->data)) {
				$message = 'Guardado.';
			} else {
				$message = 'Error.';
			}
			$this->set(array(
				'message' => $message,
				'_serialize' => array('message')
			));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Place->id = $id;
		if ($this->Place->saveField('deleted', 1)) {
			$message = 'Borrado.';
		} else {
			$message = 'Error.';
		}
		$this->set(array(
			'message' => $message,
			'_serialize' => array('message')
		));
	}


	public function isAuthorized($user) {
	    // Todos los usuarios registrados pueden agregar lugares
	    if ($this->action === 'add') {
	        return true;
	    }

	    // El dueño de un lugar puede editarlo y borrarlo
	    if (in_array($this->action, array('edit', 'delete'))) {
	        $postId = $this->request->params['pass'][0];
	        if ($this->Post->isOwnedBy($postId, $user['id'])) {
	        return true;
	        }
	    }

	    return parent::isAuthorized($user);
	}
}

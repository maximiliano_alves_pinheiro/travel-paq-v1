<?php
App::uses('AppController', 'Controller');
/**
 * Cruises Controller
 *
 * @property Cruise $Cruise
 * @property PaginatorComponent $Paginator
 */
class CruisesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Cruise->recursive = 0;
		$this->set('cruises', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cruise->exists($id)) {
			throw new NotFoundException(__('Invalid cruise'));
		}
		$options = array('conditions' => array('Cruise.' . $this->Cruise->primaryKey => $id));
		$this->set('cruise', $this->Cruise->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cruise->create();
			if ($this->Cruise->save($this->request->data)) {
				$this->Session->setFlash(__('The cruise has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cruise could not be saved. Please, try again.'));
			}
		}
		$cruiseCompanies = $this->Cruise->CruiseCompany->find('list');
		$places = $this->Cruise->Place->find('list');
		$this->set(compact('cruiseCompanies', 'places'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cruise->exists($id)) {
			throw new NotFoundException(__('Invalid cruise'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cruise->save($this->request->data)) {
				$this->Session->setFlash(__('The cruise has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cruise could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Cruise.' . $this->Cruise->primaryKey => $id));
			$this->request->data = $this->Cruise->find('first', $options);
		}
		$cruiseCompanies = $this->Cruise->CruiseCompany->find('list');
		$places = $this->Cruise->Place->find('list');
		$this->set(compact('cruiseCompanies', 'places'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cruise->id = $id;
		if (!$this->Cruise->exists()) {
			throw new NotFoundException(__('Invalid cruise'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cruise->delete()) {
			$this->Session->setFlash(__('The cruise has been deleted.'));
		} else {
			$this->Session->setFlash(__('The cruise could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

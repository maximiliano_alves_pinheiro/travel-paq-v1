<?php
App::uses('AppController', 'Controller');
/**
 * PackageTaxes Controller
 *
 * @property PackageTax $PackageTax
 * @property PaginatorComponent $Paginator
 */
class PackageTaxesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->PackageTax->recursive = 0;
		$this->set('packageTaxes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->PackageTax->exists($id)) {
			throw new NotFoundException(__('Invalid package tax'));
		}
		$options = array('conditions' => array('PackageTax.' . $this->PackageTax->primaryKey => $id));
		$this->set('packageTax', $this->PackageTax->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->PackageTax->create();
			if ($this->PackageTax->save($this->request->data)) {
				$this->Session->setFlash(__('The package tax has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The package tax could not be saved. Please, try again.'));
			}
		}
		$taxPackageKinds = $this->PackageTax->TaxPackageKind->find('list');
		$this->set(compact('taxPackageKinds'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->PackageTax->exists($id)) {
			throw new NotFoundException(__('Invalid package tax'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PackageTax->save($this->request->data)) {
				$this->Session->setFlash(__('The package tax has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The package tax could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PackageTax.' . $this->PackageTax->primaryKey => $id));
			$this->request->data = $this->PackageTax->find('first', $options);
		}
		$taxPackageKinds = $this->PackageTax->TaxPackageKind->find('list');
		$this->set(compact('taxPackageKinds'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->PackageTax->id = $id;
		if (!$this->PackageTax->exists()) {
			throw new NotFoundException(__('Invalid package tax'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PackageTax->delete()) {
			$this->Session->setFlash(__('The package tax has been deleted.'));
		} else {
			$this->Session->setFlash(__('The package tax could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

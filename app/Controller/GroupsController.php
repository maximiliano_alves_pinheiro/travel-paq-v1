<?php
App::uses('AppController', 'Controller');
/**
 * Groups Controller
 *
 * @property Group $Group
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 */
class GroupsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler');
	public $uses = array('Group');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$groups = $this->Group->find('all', array('recursive' => 0, 'conditions' => array('Group.deleted' => 0)));
		$this->set(array(
			'groups' => $groups,
			'_serialize' => array('groups')
		));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$group = $this->Group->findById($id);
		$this->set(array(
			'group' => $group,
			'_serialize' => array('group')
		));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Group->create();
			if ($this->Group->saveAll($this->request->data)) {
				$message = 'Guardado';
			} else {
				$message = 'Error.';
			}
			$this->set(array(
				'message' => $message,
				'_serialize' => array('message')
			));
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if ($this->request->is(array('post', 'put'))) {
			$this->Group->id = $id;
			if ($this->Group->saveAll($this->request->data)) {
				$message = 'Guardado.';
			} else {
				$message = 'Error.';
			}
			$this->set(array(
				'message' => $message,
				'_serialize' => array('message')
			));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Group->id = $id;
		if ($this->Group->saveField('deleted', 1)) {
			$message = 'Borrado.';
		} else {
			$message = 'Error.';
		}
		$this->set(array(
			'message' => $message,
			'_serialize' => array('message')
		));
	}

}
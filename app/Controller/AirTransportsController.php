<?php
App::uses('AppController', 'Controller');
/**
 * AirTransports Controller
 *
 * @property AirTransport $AirTransport
 * @property PaginatorComponent $Paginator
 */
class AirTransportsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AirTransport->recursive = 0;
		$this->set('airTransports', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AirTransport->exists($id)) {
			throw new NotFoundException(__('Invalid air transport'));
		}
		$options = array('conditions' => array('AirTransport.' . $this->AirTransport->primaryKey => $id));
		$this->set('airTransport', $this->AirTransport->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AirTransport->create();
			if ($this->AirTransport->save($this->request->data)) {
				$this->Session->setFlash(__('The air transport has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The air transport could not be saved. Please, try again.'));
			}
		}
		$airlines = $this->AirTransport->Airline->find('list');
		$departures = $this->AirTransport->Departure->find('list');
		$this->set(compact('airlines', 'departures'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AirTransport->exists($id)) {
			throw new NotFoundException(__('Invalid air transport'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AirTransport->save($this->request->data)) {
				$this->Session->setFlash(__('The air transport has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The air transport could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AirTransport.' . $this->AirTransport->primaryKey => $id));
			$this->request->data = $this->AirTransport->find('first', $options);
		}
		$airlines = $this->AirTransport->Airline->find('list');
		$departures = $this->AirTransport->Departure->find('list');
		$this->set(compact('airlines', 'departures'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AirTransport->id = $id;
		if (!$this->AirTransport->exists()) {
			throw new NotFoundException(__('Invalid air transport'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->AirTransport->delete()) {
			$this->Session->setFlash(__('The air transport has been deleted.'));
		} else {
			$this->Session->setFlash(__('The air transport could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

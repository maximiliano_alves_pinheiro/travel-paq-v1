<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 * @property RequestHandlerComponent $RequestHandler
 */
class CategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('RequestHandler');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$categories = $this->Category->find('all', array('recursive' => -1, 'conditions' => array('Category.deleted' => 0)));
		$this->set(array(
			'categories' => $categories,
			'_serialize' => array('categories')
		));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$category = $this->Category->findById($id);
		$this->set(array(
			'category' => $category,
			'_serialize' => array('category')
		));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Category->create();
			if ($this->Category->saveAll($this->request->data)) {
				$message = 'Guardado';
			} else {
				$message = 'Error.';
			}
			$this->set(array(
				'message' => $message,
				'_serialize' => array('message')
			));
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if ($this->request->is(array('post', 'put'))) {
			$this->Category->id = $id;
			if ($this->Category->saveAll($this->request->data)) {
				$message = 'Guardado.';
			} else {
				$message = 'Error.';
			}
			$this->set(array(
				'message' => $message,
				'_serialize' => array('message')
			));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Category->id = $id;
		if ($this->Category->saveField('deleted', 1)) {
			$message = 'Borrado.';
		} else {
			$message = 'Error.';
		}
		$this->set(array(
			'message' => $message,
			'_serialize' => array('message')
		));
	}
}

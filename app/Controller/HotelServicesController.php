<?php
App::uses('AppController', 'Controller');
/**
 * HotelServices Controller
 *
 * @property HotelService $HotelService
 * @property PaginatorComponent $Paginator
 */
class HotelServicesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->HotelService->recursive = 0;
		$this->set('hotelServices', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->HotelService->exists($id)) {
			throw new NotFoundException(__('Invalid hotel service'));
		}
		$options = array('conditions' => array('HotelService.' . $this->HotelService->primaryKey => $id));
		$this->set('hotelService', $this->HotelService->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->HotelService->create();
			if ($this->HotelService->save($this->request->data)) {
				$this->Session->setFlash(__('The hotel service has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The hotel service could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->HotelService->exists($id)) {
			throw new NotFoundException(__('Invalid hotel service'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->HotelService->save($this->request->data)) {
				$this->Session->setFlash(__('The hotel service has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The hotel service could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('HotelService.' . $this->HotelService->primaryKey => $id));
			$this->request->data = $this->HotelService->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->HotelService->id = $id;
		if (!$this->HotelService->exists()) {
			throw new NotFoundException(__('Invalid hotel service'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->HotelService->delete()) {
			$this->Session->setFlash(__('The hotel service has been deleted.'));
		} else {
			$this->Session->setFlash(__('The hotel service could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler');
	public $uses = array('User', 'Group');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('add', 'logout');
	}

	public function login() {

		if ($this->Session->read('Auth.User')) {
			$this->set(array(
				'message' => array (
					'text' => __('¡Estás conectado!'),
					'type' => 'error'
				),
				'_serialize' => array('message')
			));
		}

		if ($this->request->is('get')) {
			if ($this->Auth->login()) {
				$this->set(array(
					'user' => $this->Session->read('Auth.User'),
					'_serialize' => array('user')
				));
			} else {
				$this->set(array(
					'message' => array(
						'text' => __('Nombre de usuario o contraseña no válidos, intenta nuevamente.'),
						'type' => 'error'
					),
					'serialize' => array('message')
				));
			}
		}
	}


	public function logout() {
		if ($this->Auth->logout()) {
			$this->set(array(
				'message' => array(
					'text' => __('Desconexión satisfactoria.'),
					'type' => 'info'
				),
				'_serialize' => array('message')
			));
		}
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$users = $this->User->find('all', array('recursive' => 0, 'conditions' => array('User.deleted' => 0)));
		$this->set(array(
			'users' => $users,
			'_serialize' => array('users')
		));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->User->recursive = -1;
		$user = $this->User->findById($id);
		$this->set(array(
			'user' => $user,
			'_serialize' => array('user')
		));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->saveAll($this->request->data)) {
				$this->set(array(
					'message' => array(
						'text' => __('Registrado exitosamente.'),
						'type' => 'info',
						'status' => 'OK'
					),
					'_serialize' => array('message')
				));
			} else {
				$this->set(array(
					'message' => array(
						'text' => __('El usuario no se pudo guardar. Por favor, intenta nuevamente.'),
						'type' => 'error',
						'status' => 'ERROR'
					),
					'_serialize' => array('message')
				));
			}

		} else {
            $groups = $this->Group->find('all', array('recursive' => -1, 'conditions' => array('Group.deleted' => 0)));
            echo json_encode(array(
                'groups' => $groups
            ));
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoRender = false;
		if ($this->request->is(array('post', 'put'))) {
			$this->User->id = $id;
			if ($this->User->saveAll($this->request->data)) {
				$message = 'Guardado.';
			} else {
				$message = 'Error.';
			}
			$this->set(array(
				'message' => $message,
				'_serialize' => array('message')
			));
		} else {
            $groups = $this->Group->find('all');
            echo json_encode(array(
                'groups' => $groups
            ));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if ($this->User->saveField('deleted', 1)) {
			$message = 'Guardado.';
		} else {
			$message = 'Error.';
		}
		$this->set(array(
			'message' => $message,
			'_serialize' => array('message')
		));
	}
}
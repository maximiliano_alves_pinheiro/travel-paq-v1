<?php
App::uses('AppController', 'Controller');
/**
 * Airports Controller
 *
 * @property Airport $Airport
 * @property PaginatorComponent $Paginator
 */
class AirportsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Airport->recursive = 0;
		$this->set('airports', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Airport->exists($id)) {
			throw new NotFoundException(__('Invalid airport'));
		}
		$options = array('conditions' => array('Airport.' . $this->Airport->primaryKey => $id));
		$this->set('airport', $this->Airport->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Airport->create();
			if ($this->Airport->save($this->request->data)) {
				$this->Session->setFlash(__('The airport has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The airport could not be saved. Please, try again.'));
			}
		}
		$places = $this->Airport->Place->find('list');
		$this->set(compact('places'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Airport->exists($id)) {
			throw new NotFoundException(__('Invalid airport'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Airport->save($this->request->data)) {
				$this->Session->setFlash(__('The airport has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The airport could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Airport.' . $this->Airport->primaryKey => $id));
			$this->request->data = $this->Airport->find('first', $options);
		}
		$places = $this->Airport->Place->find('list');
		$this->set(compact('places'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Airport->id = $id;
		if (!$this->Airport->exists()) {
			throw new NotFoundException(__('Invalid airport'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Airport->delete()) {
			$this->Session->setFlash(__('The airport has been deleted.'));
		} else {
			$this->Session->setFlash(__('The airport could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

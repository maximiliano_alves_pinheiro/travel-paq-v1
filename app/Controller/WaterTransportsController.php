<?php
App::uses('AppController', 'Controller');
/**
 * WaterTransports Controller
 *
 * @property WaterTransport $WaterTransport
 * @property PaginatorComponent $Paginator
 */
class WaterTransportsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->WaterTransport->recursive = 0;
		$this->set('waterTransports', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->WaterTransport->exists($id)) {
			throw new NotFoundException(__('Invalid water transport'));
		}
		$options = array('conditions' => array('WaterTransport.' . $this->WaterTransport->primaryKey => $id));
		$this->set('waterTransport', $this->WaterTransport->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->WaterTransport->create();
			if ($this->WaterTransport->save($this->request->data)) {
				$this->Session->setFlash(__('The water transport has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The water transport could not be saved. Please, try again.'));
			}
		}
		$cruiseCompanies = $this->WaterTransport->CruiseCompany->find('list');
		$departures = $this->WaterTransport->Departure->find('list');
		$this->set(compact('cruiseCompanies', 'departures'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->WaterTransport->exists($id)) {
			throw new NotFoundException(__('Invalid water transport'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->WaterTransport->save($this->request->data)) {
				$this->Session->setFlash(__('The water transport has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The water transport could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('WaterTransport.' . $this->WaterTransport->primaryKey => $id));
			$this->request->data = $this->WaterTransport->find('first', $options);
		}
		$cruiseCompanies = $this->WaterTransport->CruiseCompany->find('list');
		$departures = $this->WaterTransport->Departure->find('list');
		$this->set(compact('cruiseCompanies', 'departures'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->WaterTransport->id = $id;
		if (!$this->WaterTransport->exists()) {
			throw new NotFoundException(__('Invalid water transport'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->WaterTransport->delete()) {
			$this->Session->setFlash(__('The water transport has been deleted.'));
		} else {
			$this->Session->setFlash(__('The water transport could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

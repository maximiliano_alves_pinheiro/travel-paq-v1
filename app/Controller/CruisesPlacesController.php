<?php
App::uses('AppController', 'Controller');
/**
 * CruisesPlaces Controller
 *
 * @property CruisesPlace $CruisesPlace
 * @property PaginatorComponent $Paginator
 */
class CruisesPlacesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CruisesPlace->recursive = 0;
		$this->set('cruisesPlaces', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CruisesPlace->exists($id)) {
			throw new NotFoundException(__('Invalid cruises place'));
		}
		$options = array('conditions' => array('CruisesPlace.' . $this->CruisesPlace->primaryKey => $id));
		$this->set('cruisesPlace', $this->CruisesPlace->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CruisesPlace->create();
			if ($this->CruisesPlace->save($this->request->data)) {
				$this->Session->setFlash(__('The cruises place has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cruises place could not be saved. Please, try again.'));
			}
		}
		$cruises = $this->CruisesPlace->Cruise->find('list');
		$places = $this->CruisesPlace->Place->find('list');
		$this->set(compact('cruises', 'places'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CruisesPlace->exists($id)) {
			throw new NotFoundException(__('Invalid cruises place'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CruisesPlace->save($this->request->data)) {
				$this->Session->setFlash(__('The cruises place has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cruises place could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CruisesPlace.' . $this->CruisesPlace->primaryKey => $id));
			$this->request->data = $this->CruisesPlace->find('first', $options);
		}
		$cruises = $this->CruisesPlace->Cruise->find('list');
		$places = $this->CruisesPlace->Place->find('list');
		$this->set(compact('cruises', 'places'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CruisesPlace->id = $id;
		if (!$this->CruisesPlace->exists()) {
			throw new NotFoundException(__('Invalid cruises place'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CruisesPlace->delete()) {
			$this->Session->setFlash(__('The cruises place has been deleted.'));
		} else {
			$this->Session->setFlash(__('The cruises place could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

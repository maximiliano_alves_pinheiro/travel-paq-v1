materialAdmin

    .run(function($templateCache,$http){
          $http.get('includes/templates.html', {cache:$templateCache});
    })

    .config(function ($stateProvider, $urlRouterProvider){

        $urlRouterProvider
            .when('list', 'views/list.html')
            .when('packageview', 'views/package-view.html')
            .when('paxes','views/paxes/index.html')
            .when('paxadd','views/paxes/add.html')
            .when('packages2', 'views/package/packages2.html')
            .when('packageadd', 'views/package/add.html')
            .when('package2addmaxi', 'views/package/package2-addmaxi.html')
            .when('places', 'views/place/index.html')
            .when('placeadd', 'views/place/add.html')
            .when('placeedit/:id', 'views/place/edit.html')
            .when('placeview/:id', 'views/place/view.html')
            .when('categories', 'views/category/index.html')
            .when('categoryadd', 'views/category/add.html')
            .when('categoryedit/:id', 'views/category/edit.html')
            .when('categoryview/:id', 'views/category/view.html')
            .when('groups', 'views/group/index.html')
            .when('groupyadd', 'views/group/add.html')
            .when('groupedit/:id', 'views/group/edit.html')
            .when('groupview/:id', 'views/group/view.html')
            .when('users', 'views/user/index.html')
            .when('useryadd', 'views/user/add.html')
            .when('useredit/:id', 'views/user/edit.html')
            .when('userview/:id', 'views/user/view.html')
            .when('results', 'views/results.html')
            .when('index', 'views/index.html')
            .when('finder', 'views/finder.html')
            .when('register', 'views/register.html')
            .otherwise("/login");


        $stateProvider

            .state ('login', {
                url:'/login',
                templateUrl: 'login.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/animate.css/animate.min.css',
                                    'vendors/bower_components/material-design-iconic-font/css/material-design-iconic-font.min.css',
                                    'css/app.min.2.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery/dist/jquery.min.js',
                                    'vendors/bower_components/bootstrap/dist/js/bootstrap.min.js',
                                    'vendors/angular/angular-route.min.js',
                                    'vendors/angular/angular-resource.min.js',
                                    'vendors/angular/angular-animate.min.js',


                                    ]
                            }
                        ])
                    }
                }
            })

            .state ('register', {
                url:'/register',
                templateUrl: 'views/register.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'css/package-view.css',
                                    'vendors/bower_components/jquery-ui/jquery-ui.css',

                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js'                                    
                                ]
                            }
                        ])
                    }
                }                
            })

            //------------------------------
            // PACKAGE VIEW
            //------------------------------
        
            .state ('packageview', {
                url: '/packageview',
                templateUrl: 'views/package-view.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'css/package-view.css',
                                    'vendors/bower_components/jquery-ui/jquery-ui.css',

                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js'                                    
                                ]
                            }
                        ])
                    }
                }
                
            })
            
            //------------------------------
            // LIST
            //------------------------------
        
            .state ('list', {
                url: '/list',
                templateUrl: 'views/list.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'css/list.css',
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'

                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })


            //------------------------------
            // PACKAGES2 LIST
            //------------------------------
        
            .state ('packages2', {
                url: '/packages2',
                templateUrl: 'views/package/packages2.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })

            //------------------------------
            // PACKAGE2 NEW
            //------------------------------
        
            .state ('packageadd', {
                url: '/packageadd',
                templateUrl: 'views/package/add.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'css/package.css',
                                    'css/package-view.css',
                                    'vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css',
                                    'vendors/bower_components/jquery-ui/jquery-ui.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/chosen_v1.4.2/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js',
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/chosen_v1.4.2/chosen.jquery.js',
                                ]
                            }
                        ])
                    }
                }
                
            })

            //------------------------------
            // PACKAGE2 NEW MAXI
            //------------------------------
        
            .state ('package2addmaxi', {
                url: '/package2addmaxi',
                templateUrl: 'views/package/package2-addmaxi.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'css/package.css',
                                    'css/package-view.css',
                                    'vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css',
                                    'vendors/bower_components/jquery-ui/jquery-ui.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/chosen_v1.4.2/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js',
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/chosen_v1.4.2/chosen.jquery.js',
                                ]
                            }
                        ])
                    }
                }
                
            })

            //------------------------------
            // PLACES LIST
            //------------------------------
        
            .state ('places', {
                url: '/places',
                templateUrl: 'views/place/index.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css',
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js',
                                ]
                            },
                        ])
                    }
                }
                
            })

            //------------------------------
            // PLACES NEW
            //------------------------------
        
            .state ('placeadd', {
                url: '/placeadd',
                templateUrl: 'views/place/add.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })

            //------------------------------
            // PLACES EDIT
            //------------------------------
        
            .state ('placeedit', {
                url: '/placeedit/:id',
                templateUrl: 'views/place/edit.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'js/controllers/placecontroller.js',
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })


            //------------------------------
            // PLACES VIEW
            //------------------------------
        
            .state ('placeview', {
                url: '/placeview/:id',
                templateUrl: 'views/place/view.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'js/controllers/placecontroller.js',
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })


            //------------------------------
            // CATEGORY LIST
            //------------------------------
        
            .state ('categories', {
                url: '/categories',
                templateUrl: 'views/category/index.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })

            //------------------------------
            // CATEGORY NEW
            //------------------------------
        
            .state ('categoryadd', {
                url: '/categoryadd',
                templateUrl: 'views/category/add.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })

            //------------------------------
            // CATEGORY EDIT
            //------------------------------
        
            .state ('categoryedit', {
                url: '/categoryedit/:id',
                templateUrl: 'views/category/edit.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })


            //------------------------------
            // CATEGORY VIEW
            //------------------------------
        
            .state ('categoryview', {
                url: '/categoryview/:id',
                templateUrl: 'views/category/view.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })


            //------------------------------
            // GROUP LIST
            //------------------------------
        
            .state ('groups', {
                url: '/groups',
                templateUrl: 'views/group/index.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
            })

            //------------------------------
            // GROUP NEW
            //------------------------------
        
            .state ('groupadd', {
                url: '/groupadd',
                templateUrl: 'views/group/add.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })

            //------------------------------
            // GROUP EDIT
            //------------------------------
        
            .state ('groupedit', {
                url: '/groupedit/:id',
                templateUrl: 'views/group/edit.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })


            //------------------------------
            // GROUP VIEW
            //------------------------------
        
            .state ('groupview', {
                url: '/groupview/:id',
                templateUrl: 'views/group/view.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })


            //------------------------------
            // USER LIST
            //------------------------------
        
            .state ('users', {
                url: '/users',
                templateUrl: 'views/user/index.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
            })

            //------------------------------
            // USER NEW
            //------------------------------
        
            .state ('useradd', {
                url: '/useradd',
                templateUrl: 'views/user/add.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })

            //------------------------------
            // USER EDIT
            //------------------------------
        
            .state ('useredit', {
                url: '/useredit/:id',
                templateUrl: 'views/user/edit.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })


            //------------------------------
            // USER VIEW
            //------------------------------
        
            .state ('userview', {
                url: '/userview/:id',
                templateUrl: 'views/user/view.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js'
                                ]
                            }
                        ])
                    }
                }
                
            })



            //------------------------------
            // RESULTS
            //------------------------------
        
            .state ('results', {
                url: '/results',
                templateUrl: 'views/results.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/fullcalendar/dist/fullcalendar.min.css',
                                    'vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css',
                                ]
                            },
                            {
                                name: 'vendors',
                                insertBefore: '#app-level-js',
                                files: [
                                    'vendors/sparklines/jquery.sparkline.min.js',
                                    'vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
                                    'vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js',
                                    'vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js',
                                ]
                            }
                        ])
                    }
                }
            })


            //------------------------------
            // INDEX
            //------------------------------

            .state ('index', {
                url: '/index',
                templateUrl: 'views/index.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/fullcalendar/dist/fullcalendar.min.css',
                                    'vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css',
                                ]
                            },
                            {
                                name: 'vendors',
                                insertBefore: '#app-level-js',
                                files: [
                                    'vendors/sparklines/jquery.sparkline.min.js',
                                    'vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
                                    'vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js',
                                    'vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js',
                                ]
                            }
                        ])
                    }
                }
            })

            //------------------------------
            // FINDER
            //------------------------------
        
            .state ('finder', {
                url: '/finder',
                templateUrl: 'views/finder.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'css/finder.css',
                                    'vendors/bower_components/jquery-ui/jquery-ui.css',

                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'vendors/fileinput/fileinput.min.js'
                                ]
                            }
                        ])
                    }
                }
                
            })


            //------------------------------
            // HOME
            //------------------------------
        
            .state ('home', {
                url: '/home',
                templateUrl: 'views/home.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/fullcalendar/dist/fullcalendar.min.css',
                                ]
                            },
                            {
                                name: 'vendors',
                                insertBefore: '#app-level-js',
                                files: [
                                    'vendors/sparklines/jquery.sparkline.min.js',
                                    'vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
                                    'vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js'
                                ]
                            }
                        ])
                    }
                }
            })
        

            //------------------------------
            // TYPOGRAPHY
            //------------------------------
        
            .state ('typography', {
                url: '/typography',
                templateUrl: 'views/typography.html'
            })


            //------------------------------
            // WIDGETS
            //------------------------------
        
            .state ('widgets', {
                url: '/widgets',
                templateUrl: 'views/common.html'
            })

            .state ('widgets.widgets', {
                url: '/widgets',
                templateUrl: 'views/widgets.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/mediaelement/build/mediaelementplayer.css',
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/mediaelement/build/mediaelement-and-player.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state ('widgets.widget-templates', {
                url: '/widget-templates',
                templateUrl: 'views/widget-templates.html'
            })


            //------------------------------
            // TABLES
            //------------------------------
        
            .state ('tables', {
                url: '/tables',
                templateUrl: 'views/common.html'
            })

            .state ('tables.tables', {
                url: '/tables',
                templateUrl: 'views/tables.html'
            })

            .state ('tables.data-tables', {
                url: '/data-tables',
                templateUrl: 'views/data-tables.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/jquery.bootgrid/dist/jquery.bootgrid.min.css',
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery.bootgrid/dist/jquery.bootgrid-override.min.js'
                                ]
                            }
                        ])
                    }
                }
            })

        
            //------------------------------
            // FORMS
            //------------------------------
            .state ('form', {
                url: '/form',
                templateUrl: 'views/common.html'
            })

            .state ('form.basic-form-elements', {
                url: '/basic-form-elements',
                templateUrl: 'views/form-elements.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/autosize/dist/autosize.min.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state ('form.form-components', {
                url: '/form-components',
                templateUrl: 'views/form-components.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css',
                                    'vendors/chosen_v1.4.2/chosen.min.css',
                                    'vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js',
                                    'vendors/chosen_v1.4.2/chosen.jquery.min.js',
                                    'vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js',
                                    'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/farbtastic/farbtastic.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js'
                                ]
                            }
                        ])
                    }
                }
            })
        
            .state ('form.form-examples', {
                url: '/form-examples',
                templateUrl: 'views/form-examples.html'
            })
        
            .state ('form.form-validations', {
                url: '/form-validations',
                templateUrl: 'views/form-validations.html'
            })
        
            
            //------------------------------
            // USER INTERFACE
            //------------------------------
        
            .state ('user-interface', {
                url: '/user-interface',
                templateUrl: 'views/common.html'
            })

            .state ('user-interface.colors', {
                url: '/colors',
                templateUrl: 'views/colors.html'
            })

            .state ('user-interface.animations', {
                url: '/animations',
                templateUrl: 'views/animations.html'
            })
        
            .state ('user-interface.box-shadow', {
                url: '/box-shadow',
                templateUrl: 'views/box-shadow.html'
            })
        
            .state ('user-interface.buttons', {
                url: '/buttons',
                templateUrl: 'views/buttons.html'
            })
        
            .state ('user-interface.icons', {
                url: '/icons',
                templateUrl: 'views/icons.html'
            })
        
            .state ('user-interface.alerts', {
                url: '/alerts',
                templateUrl: 'views/alerts.html'
            })
        
            .state ('user-interface.notifications-dialogs', {
                url: '/notifications-dialogs',
                templateUrl: 'views/notification-dialog.html'
            })
        
            .state ('user-interface.media', {
                url: '/media',
                templateUrl: 'views/media.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/mediaelement/build/mediaelementplayer.css',
                                    'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/mediaelement/build/mediaelement-and-player.js',
                                    'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                ]
                            }
                        ])
                    }
                }
            })
        
            .state ('user-interface.components', {
                url: '/components',
                templateUrl: 'views/components.html'
            })
        
            .state ('user-interface.other-components', {
                url: '/other-components',
                templateUrl: 'views/other-components.html'
            })
            
        
            //------------------------------
            // CHARTS
            //------------------------------
            
            .state ('charts', {
                url: '/charts',
                templateUrl: 'views/common.html'
            })

            .state ('charts.flot-charts', {
                url: '/flot-charts',
                templateUrl: 'views/flot-charts.html',
            })

            .state ('charts.other-charts', {
                url: '/other-charts',
                templateUrl: 'views/other-charts.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/sparklines/jquery.sparkline.min.js',
                                    'vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
                                ]
                            }
                        ])
                    }
                }
            })
        
        
            //------------------------------
            // CALENDAR
            //------------------------------
            
            .state ('calendar', {
                url: '/calendar',
                templateUrl: 'views/calendar.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/fullcalendar/dist/fullcalendar.min.css',
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/fullcalendar/dist/fullcalendar.min.js'
                                ]
                            }
                        ])
                    }
                }
            })
        
        
            //------------------------------
            // GENERIC CLASSES
            //------------------------------
            
            .state ('generic-classes', {
                url: '/generic-classes',
                templateUrl: 'views/generic-classes.html'
            })
        
            
            //------------------------------
            // PAGES
            //------------------------------
            
            .state ('pages', {
                url: '/pages',
                templateUrl: 'views/common.html'
            })
            
        
            //Profile
        
            .state ('pages.profile', {
                url: '/profile',
                templateUrl: 'views/profile.html'
            })
        
            .state ('pages.profile.profile-about', {
                url: '/profile-about',
                templateUrl: 'views/profile-about.html'
            })
        
            .state ('pages.profile.profile-timeline', {
                url: '/profile-timeline',
                templateUrl: 'views/profile-timeline.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state ('pages.profile.profile-photos', {
                url: '/profile-photos',
                templateUrl: 'views/profile-photos.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                ]
                            }
                        ])
                    }
                }
            })
        
            .state ('pages.profile.profile-connections', {
                url: '/profile-connections',
                templateUrl: 'views/profile-connections.html'
            })
        
        
            //-------------------------------
        
            .state ('pages.listview', {
                url: '/listview',
                templateUrl: 'views/list-view.html'
            })
        
            .state ('pages.messages', {
                url: '/messages',
                templateUrl: 'views/messages.html'
            })
        
            
            
            //------------------------------
            // BREADCRUMB DEMO
            //------------------------------
            .state ('breadcrumb-demo', {
                url: '/breadcrumb-demo',
                templateUrl: 'views/breadcrumb-demo.html'
            })


            //----------------------
            //  Paxes 
            //----------------------
            .state ('paxes', {
                url:'/paxes',
                templateUrl: 'views/paxes/index.html',
                             
            })
            .state ('paxadd', {
                url:'/paxadd',
                templateUrl: 'views/paxes/add.html',

                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css',
                                    'vendors/bower_components/jquery-ui/jquery-ui.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/chosen_v1.4.2/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js',
                                    'vendors/bower_components/jquery-ui/jquery-ui.js',
                                    'js/modules/components.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/chosen_v1.4.2/chosen.jquery.js',
                                ]
                            }
                        ])
                    }
                }     
            })
    });
materialAdmin

    //=================================================
    // LOGIN
    //=================================================

    .controller('loginCtrl', function($scope, $rootScope, $http, $location){
        
        //Status
        this.login = 1;
        this.register = 0;
        this.forgot = 0;


        $scope.login = function() {
            if (($scope.lctrl.username) && ($scope.lctrl.password)) {
                $rootScope.$emit('event:loginRequest', $scope.lctrl.username, $scope.lctrl.password);
                //$location.path('/login');
            } else swal('Lo sentimos!', 'Debe completar los campos.' + $scope.lctrl.username, 'error');
        };

        $scope.requestData = [];

        $http.get($rootScope.appUrl + '/users/add')
            .then(function(resp) {
                $scope.requestData = resp.data;
                console.log($scope.requestData);
            }, function(err) {
                console.error('ERR', err);
                swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
        })

        $scope.user = {};

        $scope.register = function() {
            console.log('call register...');
            var _data = {};
            _data.User = $scope.user;

            if ($scope.user.username && $scope.user.password) {
                $http.post($rootScope.appUrl + '/users.json', _data)
                    .success(function(data, status, headers, config) {
                        swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                        $location.path('/users');
                    }).error(function(data, status, headers, config) {
                        swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
                });
            } else swal('Lo sentimos!', 'Debe completar los campos.' + $scope.user.username, 'error');
        }
    })




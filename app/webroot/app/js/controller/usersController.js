materialAdmin

    //=================================================
    // USER
    //=================================================

    .controller('userListCtrl', function($scope, $rootScope, $http, $location, growlService) {
        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/users.json')
                    .success(function(data, status, headers, config) {
                        $scope.users = data.users;
                        console.log($scope.users);
                        angular.copy($scope.users, $scope.copy);
                    });
        }

        load();

        $scope.addUser = function() {
            console.log('call addUser');
            $location.path('/useradd');
        }

        $scope.editUser = function(index) {
            console.log('call editUser');
            $location.path('/useredit/' + $scope.users[index].User.id);
        }

        $scope.delUser = function(index) {
            console.log('call delUser');

            swal({
                title: "¿Está seguro?",
                text: "Está seguro que quiere eliminar este registro?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Sí, eliminarlo!",
                confirmButtonColor: "#ec6c62"
                }, function() {
                    var todel = $scope.users[index];
                    $http
                        .delete($rootScope.appUrl + '/users/' + todel.User.id + '.json')
                        .success(function(data, status, headers, config) {
                            swal('Eliminado!', 'Los datos fueron eliminados exitosamente.', 'success');
                            load();
                        }).error(function(data, status, headers, config) {
                            swal('Lo sentimos!', 'Los datos no pudieron ser eliminados.', 'error');
                        });
                });
        }

        $scope.viewUser = function(index) {
            console.log('call viewUser');
            $location.path('/userview/' + $scope.users[index].User.id);
        }

    })

    //=================================================
    // USER NEW
    //=================================================

    .controller('addUserCtrl', function($scope, $rootScope, $http, $location, growlService) {

        // var load = function() {
        //     console.log('call load()...');
        //     $http.get('http://www.travelpaq.com.ar/tpaq/users/add')
        //     .success(function(data, status, headers, config) {
        //         $scope.requestData = angular.fromJson(data);
        //         angular.copy($scope.requestData, $scope.copy);
        //         console.log($scope.requestData);

        //         // $.each($scope.requestData.groups, function (id, group){$("#userGroup").append('<option value="' + group.Group.id + '">' + group.Group.name + '</option>');});
        //         // $("#userGroup").trigger("chosen:updated");

        //     }).error(function(data, status, headers, config) {
        //         swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
        //     });
        // }
    
        // load();
        // $scope.requestData = MyService.doStuff();

        // console.log($scope.requestData);


        $scope.requestData = [];

        $http.get($rootScope.appUrl + '/users/add')
            .then(function(resp) {
                $scope.requestData = resp.data;
                console.log($scope.requestData);
            }, function(err) {
                console.error('ERR', err);
                swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
        })
        $scope.user = {};

        $scope.saveUser = function() {
            console.log('call saveUser');
            var _data = {};
            _data.User = $scope.user;

            $http.post($rootScope.appUrl + '/users.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                    $location.path('/users');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })

    //=================================================
    // USER EDIT
    //=================================================

    .controller('editUserCtrl', function($scope, $rootScope, $http, $stateParams, $location, growlService) {

        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/users/' + $stateParams['id'] + '.json')
                .success(function(data, status, headers, config) {
                    $scope.user = data.user.User;
                    console.log($scope.user);
                    angular.copy($scope.user, $scope.copy);
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
                });
        }

        load();

        $scope.user = {};

        $scope.updateUser = function() {
            console.log('call updateUser');

            var _data = {};
            _data.User = $scope.user;
            $http.put($rootScope.appUrl + '/users/' + $scope.user.id + '.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                $location.path('/users');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })


    //=================================================
    // USER VIEW
    //=================================================

    .controller('viewUserCtrl', function($scope, $rootScope, $http, $stateParams, $location, growlService) {

        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/users/' + $stateParams['id'] + '.json')
                .success(function(data, status, headers, config) {
                    $scope.user = data.user.User;
                    angular.copy($scope.user, $scope.copy);
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
                });
        }

        load();
    })
    materialAdmin
    //=================================================
    // PACKAGE ADD
    //=================================================

    .controller('addPackageCtrl', function($scope, $rootScope, $http, $location, growlService, requestData) {


        $scope.query = '';
        $scope.searchResults = [];
        $scope.searchWords = [];
        $scope.finding = false;
        $scope.chosen_place = {"PackagesPlace":""};
        $scope.Places =  {};
        $scope.PackagesPlace =  {};

        $scope.findDestination = function(keyCode) {
            if (!$scope.finding && $scope.query.length == 3 && keyCode != 8 && ((keyCode >= 65 && keyCode <= 90) || keyCode <= 32)) {
                $scope.finding = true;
                if($scope.searchWords.indexOf($scope.query) == -1){
                    $scope.searchWords.push($scope.query);
                    $("#PackagesPlace_chosen input").attr("readOnly", "readOnly");
                    $http.get('http://api.geonames.org/searchJSON?name_startsWith='+$scope.query+'&username=malves&lang=es&country=')
                    .success(function(data, status, headers, config) {

                        $scope.searchResults = data.geonames;
                        var anterior = '';
                        //$("#PackagesPlace").empty();
                        $.each($scope.searchResults, function(id, searchResult){
                            if(searchResult.name != anterior){
                                $('#PackagesPlace').append('<option value="' + searchResult.geonameId + '">' + searchResult.name + ', <small>' +searchResult.countryName + '</small></option>');
                                $scope.Places[searchResult.geonameId] = {"name":"", "countryName":""};
                                $scope.Places[searchResult.geonameId].name = searchResult.name;
                                $scope.Places[searchResult.geonameId].countryName = searchResult.countryName;

                                anterior = searchResult.name;
                            }
                        });
                        $("#PackagesPlace").trigger("chosen:updated");
                        $("#PackagesPlace_chosen input").val($scope.query);
                        $("#PackagesPlace_chosen input").removeAttr("readOnly");
                        $scope.finding = false;
                    }).error(function(data, status, headers, config) {
                        $("#PackagesPlace_chosen input").removeAttr("readOnly");
                        $("#PackagesPlace_chosen input").val("");
                        $scope.finding = false;
                        swal('Lo sentimos!', 'Los datos no pudieron ser cargados. Vuelva a intentarlo', 'error');

                    });
                } else {
                    $scope.finding = false;
                }                
            }
        }


        var load = function() {
            requestData.getAll($rootScope.appUrl + '/packages/add')
            .then(function(data){
                $scope.requestData = data;
                console.log($scope.requestData);
            }).catch(function(error){
                swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
            })


            //$http.get()
            //.success(function(data, status, headers, config) {
            //    $scope.requestData = angular.fromJson(data);
            //    angular.copy($scope.requestData, $scope.copy);
                
                //-------------Carga de los Select ---------------
                //Categories
            //    $.each($scope.requestData.categories, function (id, name){$("#CategoriesPackage").append('<option value="' + id + '">' + name + '</option>');});
            //    $("#CategoriesPackage").trigger("chosen:updated");

                //Place_Packages
                //$("#PackagesPlace_chosen input").attr("ng-model", "query");
                //$("#PackagesPlace_chosen input").attr("ng-change", "findDestination()");                
            //    $("#PackagesPlace_chosen input").on('keyup', function(event) {
            //            $scope.query = $("#PackagesPlace_chosen input").val();
            //            $scope.findDestination(event.keyCode);
            //    });

                //Service_Kinds
            //    $scope.createSelectServiceKinds();
                //------------- Carga de los Select ---------------
            //}).error(function(data, status, headers, config) {
            //    swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
            //});
        }

        load();

        $scope.packages = {};
        $scope.packages.CategoriesPackage = [];
        $scope.packages.PackagesPlace = [];
        $scope.packages.Service = [];
        $scope.packages.Day = [];
        $scope.packages.Departure = [];

       
        //Event click on daparture date
        $scope.$watch('departureDate', function(newValue, oldValue) {
            $scope.addDepartureToPackage();
        }, true);
        
        $scope.$watch('PackagesPlace', function(newValue, oldValue) {
            if((oldValue.length == null && newValue.length) || (newValue.toString().length > oldValue.toString().length)){
                //Se agrego un destino
                place_id = newValue.toString().replace(oldValue.toString(), '');
                place_id = place_id.toString().replace(',', '');

                $scope.packages.PackagesPlace.push({
                    "number_nights":"",
                    "place_id":place_id
                });

            } else {
                if(newValue.toString().length < oldValue.toString().length){
                    //Se elimino un destino
                    place_id = oldValue.toString().replace(newValue.toString(), '');
                    place_id = place_id.toString().replace(',', '');
                }

            }
            

        });

        $scope.dayOfTheItinerary = 1;

        $scope.addRuotes = false;
        $scope.sameRoute = [];
        $scope.variedRoutes = [];

        /*_________________________________________________
        
         SERVICE SECTION
        _________________________________________________*/
        $scope.addServiceToPackage  = function(){
            if($scope.services.detail != ""){
                //Validate the data
                if($scope.services.service_kind_id == null){
                    $scope.services.service_kind_id = 0;
                }
                //Add the new service
                $scope.packages.Service.push(
                    {
                        "detail":$scope.services.detail, 
                        "service_kind_id":$scope.services.service_kind_id, 
                        "visible":true
                    }
                );
                $scope.services.detail = '';
                $scope.services.service_kind_id = 0; 
                $("[data-id=ServiceKindId] .filter-option").html("Elija el tipo de servicio")

            }
        };

        $scope.deleteServiceToPackage  = function(index){

            $scope.packages.Service.splice(index, 1);
        };

        $scope.createSelectServiceKinds = function (){
            serviceKindsSelectPickers = $(".service-kinds-selectpicker");            
            $.each(serviceKindsSelectPickers, function (index, serviceKindsSelectPicker){
                if(serviceKindsSelectPicker.options.length == 1){
                    $.each($scope.requestData.serviceKinds, function (index, serviceKind){$(serviceKindsSelectPicker).append('<option value="' + serviceKind.ServiceKind.id + '">' + serviceKind.ServiceKind.name + '</option>');});
                     $(serviceKindsSelectPicker).selectpicker('refresh');
                }
            });
        };

        /*_________________________________________________
        
         DAY SECTION
        _________________________________________________*/
        $scope.addDayToPackage  = function(){
            if($scope.days.detail != ""){
                //Add the new service
                $scope.packages.Day.push(
                    {
                        "detail":$scope.days.detail, 
                        "visible":true
                    }
                );
                $scope.days.detail = '';
                $scope.dayOfTheItinerary++;
            }
        };

        $scope.deleteDayToPackage  = function(index){
            $scope.packages.Day.splice(index, 1);
            --$scope.dayOfTheItinerary;
        };

        /*_________________________________________________
        
         DEPARTURE SECTION
        _________________________________________________*/
        $scope.addDepartureToPackage  = function(){
            var isContain = false;
            $.each($scope.packages.Departure, function(i,departure) {
                if(departure.date === $scope.departureDate) {
                    isContain = true;
                } else {
                    isContain = false;
                };
            }); 

            var icon = '';
            if(!isContain && $scope.departureDate && $scope.transportKind){
                $scope.packages.Departure.push(
                    {
                        "date":$scope.departureDate,
                        "icon":$scope.transportKind,
                        "visible":true,
                        "sameRoute":true
                    }
                );
                $scope.departureDate = '';
            }
        };

        $scope.updateVariedRoute = function() {
            $scope.variedRoutes = [];
            $.each($scope.packages.Departure, function(i,departure) {if(departure.sameRoute === false) {$scope.variedRoutes.push(departure);}}); 
        }

        $scope.deleteDepartureToPackage  = function(index){
            $scope.packages.Departure.splice(index, 1);
            $.each($scope.packages.Departure, function(i,departure) {if(departure.sameRoute === false) {$scope.variedRoutes.push(departure);}}); 
        };

        $scope.addRutesToDepartures = function(){
            $addRuotes = true;
            $.each($scope.packages.Departure, function(i,departure) {if(departure.sameRoute === false) {$scope.variedRoutes.push(departure);}}); 
            if(($scope.packages.Departure.length - $scope.variedRoutes.length) < 2){
                $scope.variedRoutes = $scope.packages.Departure;
            } 
        };

            











        $scope.savePackage = function() {
            console.log('call savePackage');
            var _data = {};
            _data.Package = $scope.package2;
            $http
                .post($rootScope.appUrl + '/packages.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                    $location.path('/packages');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })

    //=================================================
    // PACKAGES2
    //=================================================

    .controller('packageListCtrl', function($scope, $rootScope, $http, $location, growlService) {
        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/packages.json')
                    .success(function(data, status, headers, config) {
                        $scope.packages = data.packages;
                        console.log($scope.packages);
                        angular.copy($scope.packages, $scope.copy);
                    });
        }

        load();

        $scope.addPackage = function() {
            console.log('call addPackage');
            $location.path('/package2add');
        }

        $scope.editPackage = function(index) {
            console.log('call editPackage');
            $location.path('/plackage2edit/' + $scope.packages[index].Package.id);
        }


        $scope.delPackage = function(index) {
            console.log('call delPackage');

            swal({
                title: "¿Está seguro?",
                text: "Está seguro que quiere eliminar este registro?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Sí, eliminarlo!",
                confirmButtonColor: "#ec6c62"
                }, function() {
                    var todel = $scope.packages[index];
                    $http
                        .delete($rootScope.appUrl + '/packages/' + todel.Package.id + '.json')
                        .success(function(data, status, headers, config) {
                            swal('Eliminado!', 'Los datos fueron eliminados exitosamente.', 'success');
                            load();
                        }).error(function(data, status, headers, config) {
                            swal('Lo sentimos!', 'Los datos no pudieron ser eliminados.', 'error');
                        });
                });
        }

        $scope.viewPackage = function(index) {
            console.log('call viewPackage');
            $location.path('/packageview/' + $scope.packages[index].Package.id);
        }

    })
   
    //=================================================
    // PACKAGE2 EDIT
    //=================================================

    .controller('editPackageCtrl', function($scope, $rootScope, $http, $stateParams, $location, growlService) {

        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/packages/edit')
                    .success(function(data, status, headers, config) {
                        $scope.requestData = angular.fromJson(data);
                        angular.copy($scope.requestData, $scope.copy);
                    }).error(function(data, status, headers, config) {
                        swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
                    });
        }

        load();

        $scope.package2 = {};

        $scope.updatePackage = function() {
            console.log('call updatePackage');

            var _data = {};
            _data.Place = $scope.package2;
            $http
                .put($rootScope.appUrl + '/packages/' + $scope.package2.id + '.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                $location.path('/places');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })

    //=================================================
    // PACKAGE2 VIEW
    //=================================================

    .controller('viewPackageCtrl', function($scope, $rootScope, $http, $stateParams, $location, growlService) {

        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/packages/' + $stateParams['id'] + '.json')
                .success(function(data, status, headers, config) {
                    $scope.package2 = data.package2.Package;
                    angular.copy($scope.package2, $scope.copy);
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
                });
        }

        load();
    })


    //=================================================
    // PACKAGE2 NEW MAXI
    //=================================================

    .controller('newPackage2MaxiCtrl', function($scope, $rootScope, $http, $location, growlService) {

        $scope.package2 = {};

        $scope.categories = {};
        $http.get($rootScope.appUrl + '/categories.json')
                .success(function(data, status, headers, config) {
                    $scope.categories = data.categories;

                    console.log($scope.categories);
                    angular.copy($scope.categories, $scope.copy);
                });

        $scope.savePackage = function() {
            console.log('call savePackage');
            var _data = {};
            _data.Package = $scope.package2;
            $http
                .post($rootScope.appUrl + '/packages.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                    $location.path('/packages');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })
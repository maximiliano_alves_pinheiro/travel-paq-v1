materialAdmin
    //=================================================
    // PLACES
    //=================================================

    .controller('placeListCtrl', function($scope, $rootScope, $http, $location, growlService) {
        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/places.json')
                    .success(function(data, status, headers, config) {
                        $scope.places = data.places;
                        angular.copy($scope.places, $scope.copy);
                    });
        }

        load();

        $scope.addPlace = function() {
            console.log('call addPlace');
            $location.path('/placeadd');
        }

        $scope.editPlace = function(index) {
            console.log('call editPlace');
            $location.path('/placeedit/' + $scope.places[index].Place.id);
        }

        $scope.delPlace = function(index) {
            console.log('call delPlace');

            swal({
                title: "¿Está seguro?",
                text: "Está seguro que quiere eliminar este registro?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Sí, eliminarlo!",
                confirmButtonColor: "#ec6c62"
                }, function() {
                    var todel = $scope.places[index];
                    $http
                        .delete($rootScope.appUrl + '/places/' + todel.Place.id + '.json')
                        .success(function(data, status, headers, config) {
                            swal('Eliminado!', 'Los datos fueron eliminados exitosamente.', 'success');
                            load();
                        }).error(function(data, status, headers, config) {
                            swal('Lo sentimos!', 'Los datos no pudieron ser eliminados.', 'error');
                        });
                });
        }

        $scope.viewPlace = function(index) {
            console.log('call viewPlace');
            $location.path('/placeview/' + $scope.places[index].Place.id);
        }

    })

    //=================================================
    // PLACE NEW
    //=================================================

    .controller('addPlaceCtrl', function($scope, $rootScope, $http, $location, growlService) {

        $scope.place = {};

        $scope.savePlace = function() {
            console.log('call savePlace');
            var _data = {};
            _data.Place = $scope.place;
            $http.post($rootScope.appUrl + '/places.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                    $location.path('/places');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })

    //=================================================
    // PLACE EDIT
    //=================================================

    .controller('editPlaceCtrl', function($scope, $rootScope, $http, $stateParams, $location, growlService) {

        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/places/' + $stateParams['id'] + '.json')
                .success(function(data, status, headers, config) {
                    $scope.place = data.place.Place;
                    angular.copy($scope.place, $scope.copy);
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
                });
        }

        load();

        $scope.place = {};

        $scope.updatePlace = function() {
            console.log('call updatePlace');

            var _data = {};
            _data.Place = $scope.place;
            $http
                .put($rootScope.appUrl + '/places/' + $scope.place.id + '.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                $location.path('/places');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })


    //=================================================
    // PLACE VIEW
    //=================================================

    .controller('viewPlaceCtrl', function($scope, $rootScope, $http, $stateParams, $location, growlService) {

        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/places/' + $stateParams['id'] + '.json')
                .success(function(data, status, headers, config) {
                    $scope.place = data.place.Place;
                    angular.copy($scope.place, $scope.copy);
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
                });
        }

        load();
    })
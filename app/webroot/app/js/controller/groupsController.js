materialAdmin

    //=================================================
    // GROUPS
    //=================================================

    .controller('groupListCtrl', function($scope, $rootScope, $http, $location, growlService) {
        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/groups.json')
                    .success(function(data, status, headers, config) {
                        $scope.groups = data.groups;
                        angular.copy($scope.groups, $scope.copy);
                    });
        }

        load();

        $scope.addGroup = function() {
            console.log('call addGroup');
            $location.path('/groupadd');
        }

        $scope.editGroup = function(index) {
            console.log('call editGroup');
            $location.path('/groupedit/' + $scope.groups[index].Group.id);
        }

        $scope.delGroup = function(index) {
            console.log('call delGroup');

            swal({
                title: "¿Está seguro?",
                text: "Está seguro que quiere eliminar este registro?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Sí, eliminarlo!",
                confirmButtonColor: "#ec6c62"
                }, function() {
                    var todel = $scope.groups[index];
                    $http
                        .delete($rootScope.appUrl + '/groups/' + todel.Group.id + '.json')
                        .success(function(data, status, headers, config) {
                            swal('Eliminado!', 'Los datos fueron eliminados exitosamente.', 'success');
                            load();
                        }).error(function(data, status, headers, config) {
                            swal('Lo sentimos!', 'Los datos no pudieron ser eliminados.', 'error');
                        });
                });
        }

        $scope.viewGroup = function(index) {
            console.log('call viewGroup');
            $location.path('/groupview/' + $scope.groups[index].Group.id);
        }

    })

    //=================================================
    // GROUPS NEW
    //=================================================

    .controller('addGroupCtrl', function($scope, $rootScope, $http, $location, growlService) {

        $scope.group = {};

        $scope.saveGroup = function() {
            console.log('call saveGroup');
            var _data = {};
            _data.Group = $scope.group;
            $http
                .post($rootScope.appUrl + '/groups.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                    $location.path('/groups');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })

    //=================================================
    // GROUPS EDIT
    //=================================================

    .controller('editGroupCtrl', function($scope, $rootScope, $http, $stateParams, $location, growlService) {

        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/groups/' + $stateParams['id'] + '.json')
                .success(function(data, status, headers, config) {
                    $scope.group = data.group.Group;
                    console.log($scope.group);
                    angular.copy($scope.group, $scope.copy);
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
                });
        }

        load();

        $scope.group = {};

        $scope.updateGroup = function() {
            console.log('call updateGroup');

            var _data = {};
            _data.Group = $scope.group;
            $http
                .put($rootScope.appUrl + '/groups/' + $scope.group.id + '.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                $location.path('/groups');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })


    //=================================================
    // GROUPS VIEW
    //=================================================

    .controller('viewGroupCtrl', function($scope, $rootScope, $http, $stateParams, $location, growlService) {

        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/groups/' + $stateParams['id'] + '.json')
                .success(function(data, status, headers, config) {
                    $scope.group = data.group.Group;
                    angular.copy($scope.group, $scope.copy);
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
                });
        }

        load();
    })
materialAdmin

    //=================================================
    // CATEGORIES
    //=================================================

    .controller('categoryListCtrl', function($scope, $rootScope, $http, $location, growlService) {
        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/categories.json')
                    .success(function(data, status, headers, config) {
                        $scope.categories = data.categories;
                        angular.copy($scope.categories, $scope.copy);
                    });
        }

        load();

        $scope.addCategory = function() {
            console.log('call addCategory');
            $location.path('/categoryadd');
        }

        $scope.editCategory = function(index) {
            console.log('call editPlace');
            $location.path('/categoryedit/' + $scope.categories[index].Category.id);
        }

        $scope.delCategory = function(index) {
            console.log('call delCategory');

            swal({
                title: "¿Está seguro?",
                text: "Está seguro que quiere eliminar este registro?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Sí, eliminarlo!",
                confirmButtonColor: "#ec6c62"
                }, function() {
                    var todel = $scope.categories[index];
                    $http
                        // .delete('http://www.travelpaq.com.ar/tpaq/categories/' + todel.Category.id + '.json')
                        .delete($rootScope.appUrl + '/categories/' + todel.Category.id + '.json')
                        .success(function(data, status, headers, config) {
                            swal('Eliminado!', 'Los datos fueron eliminados exitosamente.', 'success');
                            load();
                        }).error(function(data, status, headers, config) {
                            swal('Lo sentimos!', 'Los datos no pudieron ser eliminados.', 'error');
                        });
                });
        }

        $scope.viewCategory = function(index) {
            console.log('call viewCategory');
            $location.path('/categoryview/' + $scope.categories[index].Category.id);
        }

    })

    //=================================================
    // CATEGORY NEW
    //=================================================

    .controller('addCategoryCtrl', function($scope, $rootScope, $http, $location, growlService) {

        $scope.category = {};

        $scope.saveCategory = function() {
            console.log('call saveCategory');
            var _data = {};
            _data.Category = $scope.category;
            $http
                .post($rootScope.appUrl + '/categories.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                    $location.path('/categories');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })

    //=================================================
    // CATEGORY EDIT
    //=================================================

    .controller('editCategoryCtrl', function($scope, $rootScope, $http, $stateParams, $location, growlService) {

        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/categories/' + $stateParams['id'] + '.json')
                .success(function(data, status, headers, config) {
                    $scope.category = data.category.Category;
                    angular.copy($scope.category, $scope.copy);
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
                });
        }

        load();

        $scope.category = {};

        $scope.updateCategory = function() {
            console.log('call updateCategory');

            var _data = {};
            _data.Category = $scope.category;
            $http
                .put($rootScope.appUrl + '/categories/' + $scope.category.id + '.json', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                $location.path('/categories');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })


    //=================================================
    // CATEGORY VIEW
    //=================================================

    .controller('viewCategoryCtrl', function($scope, $rootScope, $http, $stateParams, $location, growlService) {

        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/categories/' + $stateParams['id'] + '.json')
                .success(function(data, status, headers, config) {
                    $scope.category = data.category.Category;
                    angular.copy($scope.category, $scope.copy);
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser cargados.', 'error');
                });
        }

        load();
    })



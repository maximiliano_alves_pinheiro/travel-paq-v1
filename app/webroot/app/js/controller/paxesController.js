materialAdmin

    //===============================================
    // PAX ADD
    //===============================================
    .controller('addPaxCtrl', function($scope, $http, $location, growlService) {
        $scope.pax = {};
        $scope.savePax = function() {
            console.log('call savePax');
            var _data = {};
            _data.Pax = $scope.pax;
            $http
                .post($rootScope.appUrl + '/paxes/add', _data)
                .success(function(data, status, headers, config) {
                    swal('Guardado!', 'Los datos fueron guardados exitosamente.', 'success');
                    $location.path('/paxes');
                }).error(function(data, status, headers, config) {
                    swal('Lo sentimos!', 'Los datos no pudieron ser guardados.', 'error');
            });
        }
    })


    
    //===============================================
    // PAX INDEX
    //===============================================
    .controller('indexPaxCtrl', function($scope, $rootScope, $http, $location, growlService) {
        var load = function() {
            console.log('call load()...');
            $http.get($rootScope.appUrl + '/paxes.json')
                    .success(function(data, status, headers, config) {
                        $scope.paxes = data.paxes;
                        console.log(data.paxes);
                        angular.copy($scope.paxes, $scope.copy);
                    });
        }

        load();

        $scope.addPax = function() {
            console.log('call addPax');
            $location.path('/paxadd');
        }

        $scope.editPax = function(index) {
            console.log('call editPax');
            $location.path('/paxedit/' + $scope.paxes[index].Pax.id);
        }

        $scope.delPax = function(index) {
            console.log('call delPax');

            swal({
                title: "¿Está seguro?",
                text: "Está seguro que quiere eliminar este registro?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true,
                cancelButtonText: "Cancelar",
                confirmButtonText: "Sí, eliminarlo!",
                confirmButtonColor: "#ec6c62"
                }, function() {
                    var todel = $scope.places[index];
                    $http
                        .delete($rootScope.appUrl + '/paxes/' + todel.Pax.id + '.json')
                        .success(function(data, status, headers, config) {
                            swal('Eliminado!', 'Los datos fueron eliminados exitosamente.', 'success');
                            load();
                        }).error(function(data, status, headers, config) {
                            swal('Lo sentimos!', 'Los datos no pudieron ser eliminados.', 'error');
                        });
                });
        }

        $scope.viewPax = function(index) {
            console.log('call viewPax');
            $location.path('/paxview/' + $scope.paxs[index].Pax.id);
        }

    })
var materialAdmin = angular.module('materialAdmin', ['ngResource', 'ui.router', 'angular-loading-bar', 'oc.lazyLoad']);

materialAdmin.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});

materialAdmin.run(function($rootScope) {
    // $rootScope.appUrl = 'http://www.travelpaq.com.ar/tpaq';
    $rootScope.appUrl = '/tpaq'
})